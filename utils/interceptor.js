import axios from 'axios';
import { AsyncStorage } from 'react-native';
import { URL } from '../config';
import NavigationService from './NavigationService';

export const instance = axios.create( {
  baseURL: `${ URL.host }:${ URL.port }/api`
} );

const reqInt = instance.interceptors.request.use(
  async ( config ) => {
    let token = await AsyncStorage.getItem( 'token' );
    if ( token ) {
      config.headers['Authorization'] = token;
    }
    config.headers['Content-Type'] = 'application/json';
    return config;
  }
);

const logoutError = instance.interceptors.response.use( ( response ) => {

  return response
}, async ( error ) => {
  if ( error.response && (error.response.status === 401 || error.response.status === 403 ) ) {
    await AsyncStorage.removeItem( 'token' );
    NavigationService.navigate( "Auth" );
  }
  return Promise.reject(error);
} );

axios.interceptors.request.eject( reqInt, logoutError );

export default instance;
