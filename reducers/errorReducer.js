import {
  TRANSACTION_ID_ERROR,
  IDENTIFY_ID_ERROR,
  CLOSE_WINDOW,
  QR_ERROR,
  MOUNT_ERROR,
  MARK_ERROR,
  QR_SERVER_ERROR,
  IDENTIFY_SERVER_ERROR,
  TRANSACTION_SERVER_ERROR,
  MARK_EQUIPMENT_ERROR,
  BIND_ERROR,
  CHECK_WAREHOUSE_ERROR,
  AUTH_ERROR,
  REGISTRATION_ERROR,
  WRITE_OFF_ALREADY_MOUNTED,
  WRITE_OFF_MOUNT_ERROR,
  WRITE_OFF_ALREADY_WRITTEN_OFF,
  WRITE_OFF_INVENTORY_NOT_EXISTS,
  BIND_ERROR_NOT_FOUND,
  IDENTIFY_SHARPENING_ID_ERROR,
  IDENTIFY_WAREHOUSE_ID_ERROR,
} from '../actions/types';

const initialState = {
  errors: {
    transactionIdError: false,
    identifyIdError: false,
    qrError: false,
    mountError: false,
    markingError: false,
    markingEquipmentError: false,
    qrServerError: false,
    identifyServerError: false,
    transactionServerError: false,
    bindError: false,
    checkWarehouseError: false,
    writeOffAlreadyWrittenOffError: false,
    writeOffInventoryNotExists: false,
    bindErrorNotFound: false,
  },
  description: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case IDENTIFY_ID_ERROR: {
      return {
        errors: {
          identifyIdError: true,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false,
          writeOffAlreadyMountedError: false,
        },
        description: action.payload
      };
    }

    case AUTH_ERROR: {
      return {
        errors: {
          identifyIdError: true,
          authorizationError: true
        },
        description: action.payload
      };
    }

    case REGISTRATION_ERROR: {
      return {
        errors: {
          registrationError: true,
        },
        description: action.payload
      };
    }

    case WRITE_OFF_ALREADY_MOUNTED: {
      return {
        errors: {
          writeOffAlreadyMountedError: true,
        },
        description: action.payload
      };
    }

    case WRITE_OFF_ALREADY_WRITTEN_OFF: {
      return {
        errors: {
          writeOffAlreadyWrittenOffError: true,
        },
        description: action.payload
      };
    }

    case WRITE_OFF_INVENTORY_NOT_EXISTS: {
      return {
        errors: {
          ...initialState,
          writeOffInventoryNotExists: true,
        },
        description: action.payload
      }
    }


    case WRITE_OFF_MOUNT_ERROR: {
      return {
        errors: {
          writeOffAlreadyMountedError: true,
          identifyIdError: true
        },
        description: action.payload
      };
    }

    case TRANSACTION_ID_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: true,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case QR_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: true,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case MOUNT_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: true,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case MARK_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: true,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case QR_SERVER_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: true,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case IDENTIFY_SERVER_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: true,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case TRANSACTION_SERVER_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: false,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case MARK_EQUIPMENT_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: true,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: false
        },
        description: action.payload
      };
    }
    case BIND_ERROR: {
      return {
        errors: {
          identifyIdError: false,
          transactionIdError: false,
          qrError: false,
          mountError: false,
          markingError: false,
          markingEquipmentError: true,
          qrServerError: false,
          identifyServerError: false,
          transactionServerError: false,
          bindError: true
        },
        description: action.payload
      };
    }
    case CHECK_WAREHOUSE_ERROR:{
      return{
        errors: {
          ...initialState,
          checkWarehouseError: true
        }
      }
    }
    case BIND_ERROR_NOT_FOUND:{
      return{
        errors: {
          ...initialState,
          bindErrorNotFound: true
        }
      }
    }
    case IDENTIFY_WAREHOUSE_ID_ERROR:{
      return{
        errors: {
          ...initialState,
          identifyWarehouseIdError: true
        }
      }
    }
    case IDENTIFY_SHARPENING_ID_ERROR:{
      return{
        errors: {
          ...initialState,
          identifySharpeningIdError: true
        }
      }
    }

    case CLOSE_WINDOW: {
      return initialState;
    }
    default:
      return state;
  }
}
