import {
  GET_INVENTORY,
  GET_TIME,
  INVENTORY_BIND_WAREHOUSE,
  GET_VIDEO_URL,
  GET_IF_MOUNT,
  INVENTORY_BIND_SHARPENING,
  GET_ALL_MERKERATORS,
  CHECK_WAREHOUSE,
  GET_HISTORY,
  GET_WAREHOUSES,
  GET_SHARPENINGS,
  GET_INVENTORY_FROM_WAREHOUSE,
  GET_PROMPTS_LINKS,
  GET_MOUNTING_PROMPTS
} from '../actions/types';

const initialState = {
  inventory: null,
  manualUrl: null,
  checkWarehouseResult: null,
  time: {},
  history: null,
  ifMount: null,
  sharpenings: [],
  warehouses: [],
  InventoryFromWarehouse:[],
  promptsLinks: {},
  mountingPrompts: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_INVENTORY: {
      return {
        ...state,
        inventory: action.payload
      };
    }
    case GET_TIME: {
      return {
        ...state,
        time: action.payload
      };
    }

    case GET_HISTORY: {
      return {
        ...state,
        history: action.payload
      };
    }

     case GET_IF_MOUNT: {
      return {
        ...state,
        ifMount: action.payload
      };
    }

    case GET_VIDEO_URL: {
      return {
        ...state,
        manualUrl: action.payload
      };
    }

    case CHECK_WAREHOUSE: {
      return {
        ...state,
        checkWarehouseResult: action.payload
      };
    }

    case GET_ALL_MERKERATORS: {
      return {
        ...state,
        allMarkerators: action.payload
      };
    }

    case INVENTORY_BIND_WAREHOUSE: {
      return {
        ...state,
        result: action.payload
      };
    }

    case INVENTORY_BIND_SHARPENING: {
      return {
        ...state,
        result: action.payload
      };
    }
    case GET_WAREHOUSES:{
      return {
        ...state,
        warehouses: action.payload
      }
    }
    case GET_SHARPENINGS:{
      return {
        ...state,
        warehouses: action.payload
      }
    }
    case GET_INVENTORY_FROM_WAREHOUSE:{
      return {
        ...state,
        InventoryFromWarehouse: action.payload
      }
    }

    case GET_PROMPTS_LINKS:{
      return {
        ... state,
        promptsLinks: action.payload
      }
    }

    case GET_MOUNTING_PROMPTS:{
      return {
        ... state,
        mountingPrompts: action.payload
      }
    }

    default:
      return state;
  }
}
