import { GET_EQUIPMENT, GET_VIDEO_URL } from '../actions/types';

const initialState = {
  equipment: null,
  manualUrl: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_EQUIPMENT: {
      return {
        ...state,
        equipment: action.payload
      };
    }

    case GET_VIDEO_URL: {
      return {
        ...state,
        manualUrl: action.payload
      };
    }

    default:
      return state;
  }
}


