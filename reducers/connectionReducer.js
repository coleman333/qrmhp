import { CHECK_CONNECTION } from '../actions/types';

const initialState = {
  connection: true
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CHECK_CONNECTION: {
      return {
        ...state,
        connection: action.payload
      };
    }
    default:
      return state;
  }
}
