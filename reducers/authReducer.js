import { IS_AUTH, REGISTRATION } from '../actions/types';

const initialState = {
  token: null,
  equipment: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case IS_AUTH: {
      return {
        ...state,
        equipment: action.payload
      };
    }

    case REGISTRATION: {
      return {
        ...state,
        token: action.payload.data.token
      };
    }

    default:
      return state;
  }
}


