import { MARKING_INVENTORY, MARKING_EQUIPMENT, ALL_UNMARKED_INVENTORY, MARK_INVENTORY } from '../actions/types';

const initialState = {
  marking: null,
  markingEquipment: null,
  allUnmarkedInventory: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MARKING_INVENTORY: {
      return {
        ...state,
        marking: action.payload
      };
    }

    case ALL_UNMARKED_INVENTORY: {
      return {
        ...state,
        allUnmarkedInventory: action.payload
      };
    }

    case MARK_INVENTORY: {
      return {
        ...state,
        response: action.payload
      };
    }

    case MARKING_EQUIPMENT: {
      return {
        ...state,
        markingEquipment: [action.payload]
      };
    }
    default:
      return state;
  }
}
