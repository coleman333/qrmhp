import { combineReducers } from 'redux';

import errorReducer from './errorReducer';
import inventoryReducer from './inventoryReducer';
import equipmentReducer from './equipmentReducer';
import markingReducer from './markingReducer';
import connectionReducer from './connectionReducer';
import authReducer from './authReducer';

export default combineReducers({
  auth: authReducer,
  error: errorReducer,
  inventory: inventoryReducer,
  equipment: equipmentReducer,
  marking: markingReducer,
  connection: connectionReducer,
});
