import { instance as axios } from '../utils/interceptor';
import {
  ALL_UNMARKED_INVENTORY, MARKING_INVENTORY, MARK_ERROR, MARKING_EQUIPMENT, MARK_EQUIPMENT_ERROR, MARK_INVENTORY, GET_ALL_MERKERATORS
} from './types';


export const markingAction = id => (dispatch) => {
  axios.get(`inventory/${id}/unmarked`)
    .then((res) => {
      dispatch({
        type: MARKING_INVENTORY,
        payload: res.data.AllObjectsWithIds
      });
    })
    .catch((err) => {
      dispatch({
        type: MARK_ERROR,
        payload: err.response
      });
    });
};

export const getAllUnmarkedInventoryAction = (batch) => (dispatch) => {
  axios.get(`inventory/unmarked?batch=${batch}`)
    .then((res) => {
      dispatch({
        type: ALL_UNMARKED_INVENTORY,
        payload: res.data.data.items
      });
    })
    .catch((err) => {
      dispatch({
        type: MARK_ERROR,
        payload: err.response
      });
    });
};

export const getEquipmentForUnit = id => (dispatch) => {
  axios.get(`equipment/by-unit-number/${id}`)
    .then((res) => {
      dispatch({
        type: MARKING_EQUIPMENT,
        payload: res.data
      });
    })
    .catch((err) => {
      dispatch({
        type: MARK_EQUIPMENT_ERROR,
        payload: err.response
      });
    });
};

export const getAllMarkerators = () => (dispatch) => {
  return axios.get(`markerator/connections`)
    .then((res) => {
      dispatch({
        type: GET_ALL_MERKERATORS,
        payload: res.data
      });
      return res.data;
    })
    .catch((err) => {
      dispatch({
        type: MARK_ERROR,
        payload: err.response
      });
    });
};

export const markeratorRequest = data => (dispatch) => {
  // todo create valid request when markerator will be connected
};

export const markDetail = (deviceId, inventoryId) => (dispatch) => {
  return axios.put(`inventory/${inventoryId}/mark`, {connection_id: deviceId})
    .then((res) => {
      dispatch({
        type: MARK_INVENTORY,
        payload: res.data
      });
    })
    .catch((err) => {
      dispatch({
        type: MARK_EQUIPMENT_ERROR,
        payload: err.response
      });
    });
};


export const markEquipment = id => () => {
  axios.put(`equipment/mark/${id}`)
    .catch(() => {
    });
};
