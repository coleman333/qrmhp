import { instance as axios } from '../utils/interceptor';

export const getReport = data => () => {
  console.log(data);
  return axios.post(`inventory/report-mark-by-date`, data)
    .then(() => Promise.resolve())
    .catch(() => Promise.reject());
};
export default getReport();
