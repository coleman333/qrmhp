import { BIND_ERROR, GET_EQUIPMENT, GET_VIDEO_URL, IDENTIFY_ID_ERROR, MOUNT_ERROR, QR_ERROR, WRITE_OFF_MOUNT_ERROR } from './types';
import { instance as axios } from '../utils/interceptor';

export const equipmentAction = (id, type, buttons, navigation) => (dispatch) => {

  axios.get(`equipment/${id}`)
    .then((res) => {
      if (res.data) {
        dispatch({
          type: GET_EQUIPMENT,
          payload: res.data
        });
        navigation.navigate('EquipmentDetails', { buttons });
      } else {
        switch (type) {
          case 'id':
            dispatch({
              type: IDENTIFY_ID_ERROR,
              payload: 'Something wrong'
            });
            break;
          case 'qr':
            dispatch({
              type: QR_ERROR,
              payload: 'Something wrong'
            });
            break;
          default:
            break;
        }
      }
    })
    .catch((err) => {
      switch (type) {
        case 'id':
          dispatch({
            type: IDENTIFY_ID_ERROR,
            payload: err.response
          });
          break;
        case 'qr':
          dispatch({
            type: QR_ERROR,
            payload: err.response
          });
          break;
        default:
          break;
      }
    });
};

export const mountInventory = (eqId, invId, username) => dispatch =>
  axios.post(`equipment/${eqId}/mount`, { inv_id: invId, username })
  .then((res) => {

  })
  .catch((err) => {

    if(err.response && err.response.error === 'Not Found Item'){
      dispatch({
        type: MOUNT_ERROR,
        payload: err.response
      })
    }
    else if(err.response && err.response.error === 'Equipment can\'t mounted to another equipment.'){
      dispatch({
        type: WRITE_OFF_MOUNT_ERROR,
        payload: err.response
      })
    }

    else {
      dispatch({
        type: QR_ERROR,
        payload: err.response
      });
    }
    return Promise.reject(err);
  });

export const unmountInventory = (eqId, invId, username) => dispatch =>
  axios.post(`equipment/${eqId}/unmount`, { inv_id: invId, username })
  .then((res) => {

  })
  .catch((err) => {
    dispatch({
      type: QR_ERROR,
      payload: err.response
    });
    return Promise.reject(err);
  });

export const getManualUrl = () => dispatch => {
  return axios.get( `/video-url` )
    .then((res) => {
      dispatch({
        type: GET_VIDEO_URL,
        payload: res.data
      });
    })
    .catch( ( err ) => {
      dispatch( {
        type: BIND_ERROR,
        payload: err.response
      } );
      return Promise.reject( err );
    } )
};

export const createTransaction = (eqId, invId, typeId, name) => dispatch =>
  axios.post(`inventory/${invId}/transactions`, { type_id: typeId, equipment_id: eqId, name })
  .then((res) => {

  })
  .catch((err) => {
    dispatch({
      type: MOUNT_ERROR,
      payload: err.response
    });
    return Promise.reject(err);
  });
