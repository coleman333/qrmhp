import {
  IDENTIFY_ID_ERROR,
  TRANSACTION_ID_ERROR,
  QR_ERROR,
  GET_INVENTORY,
  GET_EQUIPMENT,
  QR_SERVER_ERROR,
  IDENTIFY_SERVER_ERROR,
  TRANSACTION_SERVER_ERROR,
  GET_ALL_MERKERATORS,
  MARK_ERROR,
  GET_WAREHOUSES,
  GET_SHARPENINGS,
  IDENTIFY_WAREHOUSE_ID_ERROR,
  IDENTIFY_SHARPENING_ID_ERROR,
  GET_INVENTORY_FROM_WAREHOUSE,
  GET_PROMPTS_LINKS,
  GET_MOUNTING_PROMPTS,
} from './types';
import { instance as axios } from '../utils/interceptor';

export const getAllMarkerators = () => ( dispatch ) => {
  return axios.get( `device` )
    .then( ( res ) => {
      dispatch( {
        type: GET_ALL_MERKERATORS,
        payload: res.data
      } );
    } )
    .catch( ( err ) => {
      dispatch( {
        type: MARK_ERROR,
        payload: err.response
      } );
    } );
};

export const getInventoryFromWarehouse = (id, type, title, limit, offset) => dispatch => {
  return axios.get(`warehouse/${id}/inventories/?title=${title}&limit=${limit}&offset=${offset}`)
    .then((res)=>{
      dispatch( {
        type: GET_INVENTORY_FROM_WAREHOUSE,
        payload: res.data.data,
      } );
    })
};

export const getMountingPrompts = (id, type, title, limit, offset) => dispatch => {
  return axios.get(`inventory/${id}/equipments/?limit=${limit}&offset=${offset}`)
    .then((res)=>{
      dispatch( {
        type: GET_MOUNTING_PROMPTS,
        payload: res.data.data,
      } );
    })
};

export const getPromptsLinks = (id, type, limit, offset)=> dispatch =>{
  axios.get(`/manual/inventory/${id}`)
    .then((res)=>{
      dispatch({
        type: GET_PROMPTS_LINKS,
        payload: res.data,
      })
    })
};

export const searchWarehouseSharpeningAction = ( id, type, buttons, navigation ) => dispatch => {
  let typeAction = '';
  if ( id[0] === 'w' ) {
    typeAction = 'storing';
  }else if (id[0] === 'r'){
    typeAction = 'repair';
  }

  return axios.get( `warehouse/${ id }/inventory/?type=${ typeAction }` )
    .then( ( res ) => {
      if ( type === 'identify_warehouse_id' ) {
        dispatch( {
          type: GET_WAREHOUSES,
          payload: res.data.data,
        } );
        navigation.navigate('WarehouseDetails', { buttons, id, type });
      } else {
        dispatch( {
          type: GET_SHARPENINGS,
          payload: res.data.data,
        } );
        navigation.navigate('WarehouseDetails',{ buttons, id, type });
      }
    } )
    .catch( ( err ) => {
      if ( err.response && err.response.status !== 500 ) {
        switch ( type ) {
          case 'identify_warehouse_id': {
            dispatch( {
              type: IDENTIFY_WAREHOUSE_ID_ERROR,
              payload: err.response,
            } );
            break;
          }
          case 'identify_sharpening_id': {
            dispatch( {
              type: IDENTIFY_SHARPENING_ID_ERROR,
              payload: err.response,
            } );
            break;
          }
        }
      }
    } )
};

export const searchAction = ( id, type, buttons, navigation, identificationFlag ) => dispatch =>
  axios.get( `vendor_code_search/${ id }` )
    .then( ( res ) => {
      if ( !res.data.is_equipment ) {
        dispatch( {
          type: GET_INVENTORY,
          payload: res.data
        } );
        type = 'identify_id';
        navigation.navigate( 'InventoryDetails', { buttons, id, type, identificationFlag } );
      } else if ( res.data.is_equipment ) {
        dispatch( {
          type: GET_EQUIPMENT,
          payload: res.data
        } );
        type = 'identify_id';
        navigation.navigate( 'EquipmentDetails', { buttons, id, type } );
      }
    } )
    .catch( ( err ) => {
      if ( err.response && err.response.status !== 500 ) {
        switch ( type ) {
          case 'identify_id':
            dispatch( {
              type: IDENTIFY_ID_ERROR,
              payload: err.response
            } );
            break;
          case 'transaction_id':
            dispatch( {
              type: TRANSACTION_ID_ERROR,
              payload: err.response
            } );
            break;
          case 'qr':
            dispatch( {
              type: QR_ERROR,
              payload: err.response
            } );
            break;
          default:
            break;
        }
      } else {
        switch ( type ) {
          case 'identify_id':
            dispatch( {
              type: IDENTIFY_SERVER_ERROR,
              payload: err.response
            } );
            break;
          case 'transaction_id':
            dispatch( {
              type: TRANSACTION_SERVER_ERROR,
              payload: err.response
            } );
            break;
          case 'qr':
            dispatch( {
              type: QR_SERVER_ERROR,
              payload: err.response
            } );
            break;
          default:
            break;
        }
      }
    } );
export default null;
