import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    position: 'absolute',
    zIndex: 1
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
  },
  buttonContainer:{
    paddingBottom: '1%',
    width: '60%'
  },
  headerText: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    padding: 30,
    margin: 'auto'
  }
});
