import React, { Component } from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import { RNCamera as Camera } from 'react-native-camera'; //  todo: maybe change library to new
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ViewFinder from './ViewFinder';
import Modal from '../Modal';
import Spinner from '../ActivityIndicator';
import Button from '../Button';
import { searchAction, searchWarehouseSharpeningAction } from '../../../actions/searchAction';
import { getTime, checkIfMount } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import { unmountInventory, mountInventory, createTransaction } from '../../../actions/equipmentAction';
import styles from './styles';

class Qrscaner extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      id: '',
      modal: false,
      equalIdModal: false,
      mountModal: false,
      successModal: false,
      upsModal: false,
      serverError: false,
      inProgress: false,
      connection: true,
      connectionModal: false,
      customModal: false,
      nameModal: false,
      name: '',
      isCameraReady: true,
      isMount: true
      // isMount: false
    };

    this.wait = false;
    this.searchInfo = this.searchInfo.bind( this );
    this.mountInventory = this.mountInventory.bind( this );
    this.unmountIventory = this.unmountIventory.bind( this );
    this.mount = this.mount.bind( this );
    this.unmount = this.unmount.bind( this );
    this.goBack = this.goBack.bind( this );
    this.inputName = this.inputName.bind( this );
    this.closeModal = this.closeModal.bind( this );
    this.detectQr = this.detectQr.bind( this );
    this.customId = this.customId.bind( this );
    this.customEquipment = this.customEquipment.bind( this );
  }

  componentDidMount() {
    const { connection } = this.props;
    this.scan = true;
    this.custom = false;
    this.setState( {
      connection
    } );

    if ( this.type !== 'search' ) {
      this.setState( {
        nameModal: false
      } );
    }
  }

  //  show modal windows according to errors in store
  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      mountModal: nextProps.mountError,
      serverError: nextProps.serverError,
      connection: nextProps.connection,
      writeOffAlreadyMountedError: nextProps.writeOffAlreadyMountedError,
      writeOffAlreadyWrittenOffError: nextProps.writeOffAlreadyWrittenOffError,
      ifMount: nextProps.ifMount,
    } );
    if ( nextProps.identifySharpeningIdError || nextProps.identifyWarehouseIdError ) {
      this.props.navigation.navigate( 'Identification' );
    }
  }

  static setToLocalStorage( equipmentId, inventoryId, statusId, type ) {
    AsyncStorage.getItem( 'transaction' )
      .then( ( res ) => {
        let data = res;
        if ( data ) {
          data = JSON.parse( data );
          data.push( {
            equipmentId, inventoryId, statusId, type
          } );
          data = JSON.stringify( data );
          AsyncStorage.setItem( 'transaction', data );
        } else {
          data = JSON.stringify( [{
            equipmentId, inventoryId, statusId, type
          }] );
          AsyncStorage.setItem( 'transaction', data );
        }
      } )
      .catch();
  }

  //  when you find inventory for id, this method is being calling
  searchInfo( id ) {
    this.setState( { inProgress: true } );
    const { searchAction: search, navigation } = this.props;
    this.setState( { id } );
    const identificationFlag = this.props.navigation.getParam( 'identificationFlag' );

    if ( id.toLowerCase()[0] === 'w' || id.toLowerCase()[0] === 'r' ) {
      this.props.searchWarehouseSharpeningAction( id, 'identify_warehouse_id', false, navigation )
        .then( () => {
          this.setState( { inProgress: false, scan: true } )
        } )
    } else {
      search( id, 'qr', this.buttons, navigation, identificationFlag );
      this.setState( { inProgress: false } )
    }
  }

  // when you mount new inventory to equipment on equipment page
  mountInventory( inventoryId ) {
    const { connection, name } = this.state;
    const { mountInventory: mount, createTransaction: transaction } = this.props;
    if ( connection ) {
      this.setState( {
        id: inventoryId
      } );
      if ( !this.state.isMount ) {
        //if the inventory not mounted
        mount( this.equipment_id, inventoryId, name )
          .then( () => {
            this.setState( {
              successModal: true
            } );
          } )
          .catch( () => {
            this.wait = false;
          } );
      } else {
        this.props.checkIfMount( inventoryId )
          .then( () => {
            if ( this.state.ifMount.data[0].is_mounted ) {                      //if request returned with is_mounted = true

              this.setState( { isMountedRestrictionWindow: true } );
              return;
            } else {
              mount( this.equipment_id, inventoryId, name )                 // mount inventory to equipment
                .then( () => {
                  this.setState( {
                    successModal: true,
                    isMount: true
                  } );
                } )
                .catch( () => {
                  this.wait = false;
                } );
            }
          } )
      }
    } else {
      this.setState( {
        connectionModal: true
      } );
    }
  }

  //  when you unmount inventory from equipment on equipment page
  unmountIventory( inventoryId ) {
    const { connection, name } = this.state;
    const { navigation, } = this.props;
    const InventoryId = navigation.getParam( 'inventoryId' );
    const { unmountInventory: unmount } = this.props;
    if ( connection ) {
      this.setState( {
        id: inventoryId
      } );
      if ( !this.wait ) { // modal window isn't open
        this.wait = true;
        unmount( this.equipment_id, InventoryId, name )
          .then( () => {
            this.setState( {
              successModal: true
            } );
          } )
          .catch( () => {
            this.wait = false;
          } );
      }
    } else {
      this.setState( {
        connectionModal: true
      } );
      Qrscaner.setToLocalStorage( this.equipment_id, inventoryId, 2, 'unmount' );
    }
  }

  //  when you mount inventory to equipment on inventory page
  mount( equipmentId ) {
    const { connection, name } = this.state;
    const { mountInventory: mount } = this.props;
    if ( connection ) {
      this.setState( {
        id: equipmentId
      } );
      if ( !this.wait ) { // modal window isn't open
        this.wait = true;
        mount( equipmentId, this.inventoryId, name ) // mount inventory to equipment
          .then( () => {
            this.setState( {
              successModal: true
            } );
          } )
          .catch( () => {
            this.wait = false;
          } );
      }
    } else {
      this.setState( {
        connectionModal: true
      } );
    }
  }

  //  when you unmount inventory from equipment on inventory page
  unmount( equipmentId ) {
    const { connection, name } = this.state;
    const { unmountInventory: unmount } = this.props;
    if ( equipmentId === `${ this.equipment_id.toLowerCase() }` || equipmentId === 'e100000' ) { // clicked and scanned id are equal
      if ( connection ) {
        this.setState( {
          id: equipmentId
        } );
        if ( !this.wait ) { // modal window isn't open
          this.wait = true;
          //   create transaction for mounting inventory
          unmount( equipmentId, this.inventoryId, name )
            .then( () => {
              this.setState( {
                successModal: true
              } );
            } )
            .catch( () => {
              this.wait = false;
            } );
        }
      } else {
        this.setState( {
          connectionModal: true
        } );
        Qrscaner.setToLocalStorage( equipmentId, this.inventoryId, 2, 'unmount' );
      }
    } else {
      this.setState( {
        equalIdModal: true //   open modal window if ids are not equal
      } );
    }
  }

  //  close success modal and go back to main page
  goBack() {
    const { searchAction: search, navigation } = this.props;
    let id = '';
    if ( this.type === 'mount' || this.type === 'unmount' ) {
      id = this.inventoryId.toLowerCase();
    } else if ( this.type === 'mountInventory' || this.type === 'unmountInventory' ) {
      id = this.equipment_id.toLowerCase();
    }
    this.closeModal( 'success' );
    search( id, 'qr', true, navigation ); //    get new list of inventory on equipment
  }

  inputName( name ) {
    this.setState( {
      name
    } );
  }

  //  close modal button
  closeModal( param ) {
    const { name } = this.state;
    const { closeError: closeModal } = this.props;
    if ( param === 'name' && name.length ) {
      this.setState( {
        nameModal: false
      } );
      this.scan = true;
    } else {
      if ( param !== 'success' ) {
        this.scan = true;
      }
      this.wait = false;
      this.custom = false;
      closeModal();
      this.setState( {
        equalIdModal: false,
        mountModal: false,
        successModal: false,
        upsModal: false,
        inProgress: false,
        connectionModal: false,
        customModal: false,
        writeOffWindow: false,
        identifySharpeningIdError: false,
        identifyWarehouseIdError: false,
      } );
    }
  }

  //  when you scan QR-code, this method call function according to type input data
  detectQr( id ) {
    if ( id ) {
      id = id.trim();
    }

    this.closeModal();
    if ( id ) {
      switch ( this.type ) {
        case 'search':
          this.searchInfo( id );
          break;
        case 'mountInventory':
          this.mountInventory( id );
          break;
        case 'unmountInventory':
          this.unmountIventory( id );
          break;
        case 'mount':
          this.mount( id );
          break;
        case 'unmount':
          this.unmount( id );
          break;
        default:
          break;
      }
    }
  }

  customId() {
    this.custom = true;
    this.setState( {
      customModal: true
    } );
  }

  refusedIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
    } );
  }

  confirmIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
      isMount: false,
    }, () => this.mountInventory( this.state.id ) );
  }


  customEquipment() {
    this.detectQr( 'e100000' ); //    if user hasn't equipment id, inventory is being mounting on a specific equipment
  }

  turnTorch = () => {
    if ( this.state.torch === Camera.Constants.FlashMode.torch ) {
      this.setState( { torch: Camera.Constants.FlashMode.off, flash: Camera.Constants.FlashMode.off } )
    } else {
      this.setState( { torch: Camera.Constants.FlashMode.torch, flash: Camera.Constants.FlashMode.torch } )
    }
  };

  render() {
    const { navigation } = this.props;
    const {
      serverError, connectionModal, upsModal, modal, id, equalIdModal, mountModal, successModal, customModal,
      nameModal, inProgress, torch, flash, AlreadyMountedWarning, isMountedRestrictionWindow, writeOffAlreadyWrittenOffError,
    } = this.state;
    this.equipment_id = navigation.getParam( 'equipment_id' );
    this.inventoryId = navigation.getParam( 'inventoryId' );
    this.type = navigation.getParam( 'type' );
    this.buttons = navigation.getParam( 'buttons' );
    const text = navigation.getParam( 'text' );

    return (
      <View
        style={ { flex: 1 } }
      >
        <Modal
          closeModal={ this.refusedIsMountedRestrictionWindow.bind( this ) }
          confirmModal={ this.confirmIsMountedRestrictionWindow.bind( this ) }
          visible={ isMountedRestrictionWindow }
          type="confirm"
        >
          {
            `Внимание, данное ТМЦ установлено на оборудование. В случае осуществления вами операции, оно будет демонтировано автоматически. Продолжить?`
          }
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ writeOffAlreadyWrittenOffError }
          type="alert"
        >
          Вы не можете монтировать данное ТМЦ, т.к. оно списано с производства
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ AlreadyMountedWarning }
          type="alert"
        >
          Вы уверенны что хотите монтировать данное ТМЦ, т.к. оно установлено на другое оборудование
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ serverError }
          type="alert"
        >
          Сервер не отвечает
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ connectionModal }
          type="alert"
        >
          Действие будет выполнено, когда появится интернет соединение!
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ upsModal }
          type="alert"
        >
          {
            this.type === 'mount' ? `ТМЦ возможно установить только на оборудование` : `На оборудование возможно установить только ТМЦ`
          }
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ modal }
          type="alert"
        >
          {
            `Идентификатор ${ id } не найден в базе!`
          }
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ equalIdModal }
          type="alert"
        >
          Идентификатор не соответствует выбраному
        </Modal>
        <Modal
          closeModal={ this.closeModal }
          visible={ mountModal }
          type="alert"
        >
          Деталь смонтирована ранее на другое оборудование
        </Modal>
        <Modal
          closeModal={ this.goBack }
          visible={ successModal }
          type="success"
        >
          {
            this.type === 'mount' || this.type === 'mountInventory' ? `Монтировано успешно` : `Демонтировано успешно`
          }
        </Modal>
        <Modal
          placeholder="введите идентификатор"
          closeModal={ Id => this.detectQr( Id ) }
          visible={ customModal }
          type="custom"
        />
        <Modal
          placeholder="введите имя пользователя"
          onChange={ name => this.inputName( name ) }
          closeModal={ () => this.closeModal( 'name' ) }
          visible={ nameModal }
          type="custom"
        />
        {
          inProgress
            ? <Spinner/>
            : (
              <Camera
                flashMode={ torch }
                style={ { flex: 1 } }
                onBarCodeRead={ ( { data } ) => {
                  if ( this.scan && !this.custom ) {
                    this.setState( {
                      inProgress: true
                    } );
                    this.detectQr( data );
                    this.scan = false;
                  }
                } }
              >
                <View
                  style={ styles.header }
                >
                  <Text style={ styles.headerText }>
                    { text }
                  </Text>
                </View>

                <ViewFinder/>
                <View style={ styles.buttonsContainer }>
                  <View style={ styles.buttonContainer }>
                    <Button onClick={ this.turnTorch }> освещение </Button>
                  </View>
                  {
                    this.type !== 'search'
                      ? (
                        <React.Fragment>
                          <View style={ styles.buttonContainer }>
                            <Button onClick={ this.customId }> Ввести код вручную </Button>
                          </View>
                        </React.Fragment>
                      ) : null
                  }
                </View>

              </Camera>
            )
        }
      </View>
    );
  }
}

Qrscaner.defaultProps = {
  error: false,
  closeError: false,
  searchAction: false,
  connection: false,
  mountError: false,
  serverError: false,
  navigation: {},
  getTime: false,
  mountInventory: false,
  unmountInventory: false,
  createTransaction: false
};

Qrscaner.propTypes = {
  error: PropTypes.bool,
  closeError: PropTypes.func,
  searchAction: PropTypes.func,
  connection: PropTypes.bool,
  mountError: PropTypes.bool,
  serverError: PropTypes.bool,
  navigation: PropTypes.instanceOf( Object ),
  mountInventory: PropTypes.func,
  unmountInventory: PropTypes.func,

  createTransaction: PropTypes.func,
  getTime: PropTypes.func
};

const mapStateToProps = state => ({
  error: state.error.errors.qrError,
  mountError: state.error.errors.mountError,
  serverError: state.error.errors.qrServerError,
  connection: state.connection.connection,
  ifMount: state.inventory.ifMount,
  writeOffAlreadyMountedError: state.error.errors.writeOffAlreadyMountedError,
  writeOffAlreadyWrittenOffError: state.error.errors.writeOffAlreadyWrittenOffError,
  identifyWarehouseIdError: state.error.errors.identifyWarehouseIdError,
  identifySharpeningIdError: state.error.errors.identifySharpeningIdError,
});

export default connect( mapStateToProps, {
  closeError,
  unmountInventory,
  mountInventory,
  createTransaction,
  searchAction,
  getTime,
  checkIfMount,
  searchWarehouseSharpeningAction
} )( Qrscaner );
