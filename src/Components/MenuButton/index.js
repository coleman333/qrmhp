import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import Colors from '../../Constants/Colors';
import styles from './styles';
const menuIcon = '../../../assets/img/menu.png';

export const MenuButton = (props) => {
  const { onClick } = props;
  return (
    <TouchableOpacity
      style={styles.menuButton}
      onPress={() => {
        onClick();
      }}
    >
      <Icon
        name={"menu"}
        size={30}
        color={Colors.gray}
        type={'material'}
      />
    </TouchableOpacity>
  );
};

export default MenuButton;

MenuButton.defaultProps = {
  onClick () {

  }
};

MenuButton.propTypes = {
  onClick: PropTypes.func
};
