import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import Colors from '../../Constants/Colors';

import styles from './styles';

export default props => (
  <TouchableOpacity
    style={styles.menuButton}
    onPress={() => { props.onClick(); }}
  >
    <Icon
      name="settings"
      size={30}
      color={Colors.gray}
    />
  </TouchableOpacity>
);
