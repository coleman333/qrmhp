import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';
import styles from './styles';

export const Button = (props) => {
  const {
    onClick, buttonStyle, textStyle, children, enableButton,
  } = props;
  return (
    <TouchableOpacity
      onPress={() => (onClick())}
      style={[styles.button, buttonStyle]}
      disabled = {enableButton}
    >
      <Text
        style={[styles.textButton, textStyle]}
      >
        {children}
      </Text>
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  onClick () {

  },
  buttonStyle: {},
  textStyle: {},
  children: ''
};

Button.propTypes = {
  onClick: PropTypes.func,
  buttonStyle: PropTypes.instanceOf(Object),
  textStyle: PropTypes.instanceOf(Object),
  children: PropTypes.string
};
export default Button;
