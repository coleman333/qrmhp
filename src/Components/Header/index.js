import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import MenuButton from '../MenuButton';
import Modal from '../Modal';
import styles from './styles';

class Header extends Component {
  constructor (props) {
    super(props);
    this.state = {
      modal: false
    };
    this.openSidebar = this.openSidebar.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  openSidebar () {
    const { navigation } = this.props;
    navigation.openDrawer();
  }

  toggleModal () {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render () {
    const { children  } = this.props;
    let title ='';
    {this.props.title && (title = this.props.title) }
    const { modal } = this.state;
    return (
      <View
        style={styles.header}
      >
        <Modal
          closeModal={() => this.toggleModal()}
          visible={modal}
          type="prompt"
        >
                    Введите настройки для маркиратора
        </Modal>

        {this.props.page !== 'auth'&& this.props.page !== 'history'&& <MenuButton
          onClick={() => this.openSidebar()}
        />}
        {
          this.props.headerType === 'MountingPromptsPage' &&
            <View style={ {
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
            } }>
              <Text style={styles.headerForMountingPromptsPage}>
                {children}
              </Text>
              <Text>
                {this.props.title}
              </Text>
            </View>
        }
        {this.props.headerType !== 'MountingPromptsPage' &&
          <Text
            style={ styles.headerText }
          >
            { children }
          </Text>
        }
      </View>
    );
  }
}

Header.defaultProps = {
  navigation: {},
  children: ''
};

Header.propTypes = {
  navigation: PropTypes.instanceOf(Object),
  children: PropTypes.string
};

export default withNavigation(Header);
