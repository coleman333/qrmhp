import React from 'react';
import { SafeAreaView, Image, View } from 'react-native';
import { DrawerItems, StackActions, NavigationActions } from 'react-navigation';

export default props => (
  <SafeAreaView style={{ flex: 1, backgroundColor: 'white', borderBottomRightRadius: 25, borderTopRightRadius: 25 }}>
    <View>
    <Image style={{marginLeft:40, width: 120, height: 60}} source={require('../../../assets/img/logo.png')} />
      <View style={{ marginLeft: 10, marginRight: 10, borderBottomColor: 'black', borderBottomWidth: 1}}/>
      <DrawerItems
        {...props}
        onItemPress={
          (router) => {
            console.log('new screen');
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: router.route.routeName })]
            });
            const navigateAction = NavigationActions.navigate({
              routeName: router.route.routeName
            });
            props.navigation.dispatch(navigateAction);
            props.navigation.dispatch(resetAction);
          }
        }
      />
    </View>
  </SafeAreaView>
);
