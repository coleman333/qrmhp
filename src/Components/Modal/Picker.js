import React, { Component } from 'react';
import {
  View, Text, Picker, Platform, PickerIOS
} from 'react-native';

import Svg, { G, Path } from 'react-native-svg';
import PropTypes from 'prop-types';
import styles from './styles';
import { Button } from '../Button';
import Colors from '../../Constants/Colors';

export default class _Picker extends Component {
  constructor (props) {
    super(props);
    this.state = {
      id: '',
      pickerArray: this.props.pickerArray
    };
    this.confirm = this.confirm.bind(this);
    this.getValue = this.getValue.bind(this);
  }

  getValue (e) {
    const { onChange } = this.props;
    if (onChange) onChange(e);
    this.setState({
      id: e
    });
  }

  confirm () {
    const { onClick } = this.props;
    let { id } = this.state;
    onClick(id);
  }

  render () {
    const { children, pickerArray } = this.props;
    console.log('this is from picker modal', pickerArray);
    return (
      <View
        style={styles.prompt}
      >
        {Platform.OS === 'ios' &&<View
          style={styles.imageBlockIos}
        >
          <Svg viewBox="0 0 52 52" width="60" height="60">
            <G>
              <Path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" fill="#e47b7b" />
              <Path d="M26,10c-0.552,0-1,0.447-1,1v22c0,0.553,0.448,1,1,1s1-0.447,1-1V11C27,10.447,26.552,10,26,10z" fill="#e47b7b" />
              <Path d="M26,37c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C27,37.447,26.552,37,26,37z" fill="#e47b7b" />
            </G>
          </Svg>
        </View>}
        {Platform.OS === 'android' &&<View
          style={styles.imageBlock}
        >
          <Svg viewBox="0 0 52 52" width="90" height="90">
            <G>
              <Path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" fill="#e47b7b" />
              <Path d="M26,10c-0.552,0-1,0.447-1,1v22c0,0.553,0.448,1,1,1s1-0.447,1-1V11C27,10.447,26.552,10,26,10z" fill="#e47b7b" />
              <Path d="M26,37c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C27,37.447,26.552,37,26,37z" fill="#e47b7b" />
            </G>
          </Svg>
        </View>}
        <View
          style={styles.textBlock}
        >
          <Text
            style={styles.text}
          >
            {children}
          </Text>
        </View>
        {Platform.OS === 'android' &&
        <View
          style={{ flex: 1 , height: 40, width: "100%", borderColor: 'black', borderRadius: 10, borderWidth: 1}}
        >
          <Picker
            selectedValue={ this.state.userName }
            style={ { flex: 1,
              // width: "95%",
              height: 50, borderColor: 'black', borderRadius: 20, borderWidth: 1 } }
            mode='dialog'
            onValueChange={ ( itemValue, itemIndex ) => {
              this.setState({
                userName: itemValue
              });
              this.getValue( itemValue )
            }
            }>
            {
              pickerArray.map((item, index)=>{
                return <Picker.Item key={index} pickerStyleType={{ flex: 1 }} label={item} value={item}/>
              })
            }
          </Picker>
        </View>
        }
        {Platform.OS === 'ios' &&
        <View
          style={{ height: '80%' , width: "100%"}}
        >
          <PickerIOS
            selectedValue={ this.state.userName }
            style={ { flex: 1,
              height: 200 ,

            } }

            onValueChange={ ( itemValue, itemIndex ) => {
              this.setState({
                userName: itemValue
              });
              this.getValue( itemValue )
            }
            }>
            {
              pickerArray.map((item,index)=>{
                return <Picker.Item key={index} pickerStyleType={{ flex: 1 }} label={item} value={item}/>
              })
            }
          </PickerIOS>
        </View>
        }
        {Platform.OS === 'ios'&& <View
          style={styles.buttonBlockIos}
        >
           <Button
            onClick={this.confirm}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.success
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            OK
          </Button>
        </View>}
        {Platform.OS === 'android'&& <View
          style={styles.buttonBlock}
        >
          <Button
            onClick={this.confirm}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.success
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            OK
          </Button>
        </View>}
      </View>
    );
  }
}

_Picker.defaultProps = {
  onClick () {
  },
  onChange () {

  },
  children: '',
  placeholder: ''
};

_Picker.propTypes = {
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  children: PropTypes.string,
  placeholder: PropTypes.string
};
