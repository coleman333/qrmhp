import React, { Component } from 'react';
import {
  View, Modal, Dimensions, KeyboardAvoidingView
} from 'react-native';
import PropTypes from 'prop-types';

import Alert from './Alert';
import Confirm from './Confirm';
import Success from './Success';
import Prompt from './Prompt';
import Custom from './Custom';
import styles from './styles';
import _Picker from './Picker';
import SuccessConfirm from "./SuccessConfirm";

export default class CustomModal extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      visible: false,
      type: null
    };
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      visible: nextProps.visible,
      type: nextProps.type
    } );
  }

  static getHeight() {
    const { height } = Dimensions.get( 'screen' );
    if ( height <= 570 ) {
      return height * 0.7;
    }
    if ( height > 570 && height <= 639 ) {
      return height * 0.6;
    }
    return height * 0.5;
  }

  render() {
    const { visible, type } = this.state;
    const {
      closeModal, children, confirmModal, onChange, placeholder
    } = this.props;
    return (
      <Modal
        onRequestClose={ () => {
        } }
        animationType="fade"
        transparent
        visible={ visible }
      >
        <View
          activeOpacity={ 1.0 }
          onPress={ () => (closeModal ? closeModal() : null) }
          style={ styles.opacity }
        >
          <KeyboardAvoidingView
            behavior="position"
            style={ { alignItems: 'center', justifyContent: 'center' } }
          >
            <View
              style={ [styles.window, type === 'prompt'
                ? { height: Dimensions.get( 'window' ).height * 0.6, width: Dimensions.get( 'window' ).width * 0.8 }
                : { height: CustomModal.getHeight(), width: Dimensions.get( 'window' ).width * 0.8 }] }
            >
              {
                type === 'alert'
                  ? (
                    <Alert
                      onClick={ () => (closeModal ? closeModal() : null) }
                    >
                      { children }
                    </Alert>
                  )
                  : type === 'confirm'
                  ? (
                    <Confirm
                      onCancel={ () => (closeModal ? closeModal() : null) }
                      onClick={ () => (confirmModal ? confirmModal() : null) }
                    >
                      { children }
                    </Confirm>
                  )
                  : type === 'success'
                    ? (
                      <Success
                        onClick={ () => (closeModal ? closeModal() : null) }
                      >
                        { children }
                      </Success>
                    ) : type === 'prompt'
                      ? (
                        <Prompt
                          onClick={ () => (closeModal ? closeModal() : null) }
                        >
                          { children }
                        </Prompt>
                      ) : type === 'successConfirm'
                        ? (
                          <SuccessConfirm
                            onCancel={ () => (closeModal ? closeModal() : null) }
                            onClick={ () => (confirmModal ? confirmModal() : null) }
                          >
                            { children }
                          </SuccessConfirm>
                        )
                        : type === 'custom'
                          ? (
                            <Custom
                              placeholder={ placeholder }
                              onChange={ name => (onChange ? onChange( name ) : null) }
                              onClick={ id => (closeModal ? closeModal( id ) : null) }
                            >
                              { children }
                            </Custom>
                          ) : type === 'picker'
                            ? (
                              <_Picker style={ { height: 800 } }
                                       pickerArray={ this.props.pickerArray }
                                       placeholder={ placeholder }
                                       onChange={ name => (onChange ? onChange( name ) : null) }
                                       onClick={ id => (closeModal ? closeModal( id ) : null) }
                              >
                                { children }
                              </_Picker>
                            ) : null
              }
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    );
  }
}

CustomModal.defaultProps = {
  visible: false,
  type: '',
  closeModal() {

  },
  confirmModal() {

  },
  onChange() {

  },
  placeholder: '',
  children: ''
};

CustomModal.propTypes = {
  visible: PropTypes.bool,
  type: PropTypes.string,
  closeModal: PropTypes.func,
  confirmModal: PropTypes.func,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  children: PropTypes.string
};
