import { StyleSheet } from 'react-native';

import Colors from '../../Constants/Colors';

export default StyleSheet.create({
  opacity: {
    backgroundColor: 'rgba(0,0,0,.8)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  window: {
    borderRadius: 20,
    padding: 20,
    borderColor: '#000',
    borderWidth: 1,
    backgroundColor: '#fff'
  },
  confirm: {
    flex: 1
  },
  alert: {
    flex: 1
  },
  success: {
    flex: 1
  },
  prompt: {
    flex: 1
  },
  buttonBlock: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  imageBlock: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textBlock: {
    justifyContent: 'center',
    flex: 1
  },
  text: {
    color: Colors.gray,
    textAlign: 'center'
  },
  input: {
    height: 40,
    borderWidth: 1,
    textAlign: 'center',
    color: Colors.gray,
    borderColor: Colors.grayBorder,
    marginVertical: 5,
    borderRadius: 20
  }
});
