import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';

import Identification from '../Views/Identification';
import Marking from '../Views/Marking';
import Qrscaner from '../Components/Qrscaner';
import Transaction from '../Views/Transaction';
import InventoryDetails from '../Views/InventoryDetails';
import CustomDrawer from '../Components/CustomDrawer';
import colors from '../Constants/Colors';
import EquipmentDetails from '../Views/EquipmentDetails';
import Stock from '../Views/Stock';
import Sharpening from '../Views/Stock';
import HistoryPage from '../Views/History';
import CommonPageWarehouse from '../Views/CommonPage';
import CommonPageSharpening from '../Views/CommonPage';
import WritingOffPage from '../Views/WritingOffPage';
import WriteOffCamera from '../Views/WriteOffCamera';
import WarehouseDetails from '../Views/WarehouseDetails';
import InventoriesFromWarehouse from '../Views/InventoriesFromWarehouse';
import LinksOnVideoPrompts from '../Views/LinksOnVideoPrompts';
import MountingPrompts from '../Views/MountingPromptsPage';


const stockNavigation = createStackNavigator({
    Transaction,
    // Stock
  },
  {
    headerMode: 'none'
  });


const identifyNavigator = createStackNavigator(
  {
    Identification,
    InventoryDetails,
    EquipmentDetails,
    WarehouseDetails,
    InventoriesFromWarehouse,
  },
  {
    initialRouteName: 'Identification',
    headerMode: 'none'
  }
);

const markingNavigator = createStackNavigator({
    Marking
},
{
  headerMode: 'none'
});

const transactionNavigator = createStackNavigator({
  Transaction,
  InventoryDetails,
  EquipmentDetails,
},
{
  headerMode: 'none'
});

const CommonPageForWarehouses = createStackNavigator({
    CommonPageWarehouse
  },
  {
    headerMode: 'none'
  });

const CommonPageForSharpening = createStackNavigator({
    CommonPageSharpening
  },
  {
    headerMode: 'none'
  });
const WritingOffPageNavigation = createStackNavigator({
    WritingOffPage,
    WriteOffCamera
  },
  {
    headerMode: 'none'
  });

const drawerNavigator = createDrawerNavigator(
  {
    Identification: {
      screen: identifyNavigator,
      navigationOptions: {
        drawerLabel: 'Идентификация ТМЦ'
      }
    },
    Marking: {
      screen: markingNavigator,
      navigationOptions: {
        drawerLabel: 'Маркировка ТМЦ'
      }
    },
    Transaction: {
      screen: transactionNavigator,
      navigationOptions: {
        drawerLabel: 'Монтаж/Демонтаж'
      }
    },
    CommonPageWarehouse: {
      screen: CommonPageForWarehouses,
      navigationOptions: {
        drawerLabel: 'Принять на склад'
      }
    },
    CommonPageSharpening: {
      screen: CommonPageForSharpening,
      navigationOptions: {
        drawerLabel: 'Принять на заточку'
      }
    },
    WritingOffPage: {
      screen: WritingOffPageNavigation,
      navigationOptions: {
        drawerLabel: 'Списать материал'
      }
    }

  },
  {
    contentComponent: CustomDrawer,
    drawerWidth: 230,
    drawerBackgroundColor: 'rgba(0,0,0,.0)',

    contentOptions: {
      activeTintColor: colors.white,
      inactiveTintColor: colors.gray,
      activeBackgroundColor: colors.mhp_blue
    }
  }
);

const mainNavigator = createStackNavigator(
  {
    DrawerNavigator: drawerNavigator,
    Qrscaner,
    Stock,
    Sharpening,
    Transaction,
    HistoryPage,
    MountingPrompts,
    LinksOnVideoPrompts,
  },
  {
    headerMode: 'none'
  }
);

export default createAppContainer(mainNavigator);
