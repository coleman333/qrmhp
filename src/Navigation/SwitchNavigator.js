import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import mainNavigator from "./Router";
import  AuthNavigation  from "./AuthNavigation";

const SwitchNavigator = createSwitchNavigator({
    // splashScreen: drawerNavigator

    Auth: AuthNavigation,
    mainNavigator: mainNavigator
  },{
    navigationOptions: {
      header: null,
    },
  }
);

export default createAppContainer(SwitchNavigator);
