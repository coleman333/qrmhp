import React, { Component } from 'react';
import { Text, SafeAreaView, View, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import _Button from '../../Components/Button';
import _Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import styles from './styles';
import { getEquipmentForUnit, markEquipment, markeratorRequest } from '../../../actions/markingAction';
import { closeError } from '../../../actions/errorAction';

class MarkingEquipment extends Component{

    state = {
        modal: false,
        confirmModal: false,
        confirmMarkModal: false,
        inProgress: false,
        remarkModal: false,
        id: '4102491', //   todo remove value
        item: null,
        items: [],
        port: '',
        host: '',
        currentIndex: null
    };

    componentWillReceiveProps(nextProps) {
        this.markEquipment = nextProps.marking;
        this.setState({
            modal: nextProps.error,
            items: nextProps.marking,
            inProgress: false
        });
    };

    //  close modal windows
    closeModal = () => {
        this.props.closeError();
        this.setState({
            confirmModal: false,
            nextModal: false,
            confirmMarkModal: false,
            remarkModal: false,
            successModal: false
        });
    };

    //  open modal window to confirm marking details
    openConfirmModal = (item, index) => {
        this.setState({
            confirmModal: true,
            item: item,
            currentIndex: index
        });
    };

    //  resolve action for marking detail
    resolveConfirmModal = () => {
        AsyncStorage.getItem('host')
            .then(data => {
                if (data !== null)
                    this.setState({
                        host: data
                    });
                AsyncStorage.getItem('port')
                    .then(data => {
                        if (data !== null)
                            this.setState({
                                port: data
                            });
                        this.markerator({port: this.state.port, host: this.state.host, message: this.state.item.id}); //    request for marking detail on markerator
                    })
                    .catch(err => {

                    });
            })
            .catch(err => {

            });
        this.setState({
            inProgress: true
        });
        this.closeModal();
            this.setState({
                inProgress: false
            });
            this.openConfirmMarkModal();
    };

    //  open modal to confirm success marking
    openConfirmMarkModal = () => {
        this.setState({
             confirmMarkModal: true
        });
    };

    //  resolve success marking
    resolveConfirmMarkModal = () => {
        this.markEquipment.splice(this.state.currentIndex, 1);
        this.setState({
            items: this.markEquipment
        });
        this.mark(this.state.item.id);
        this.closeModal();
    };

    //  open modal window if marking not success and try remark detail
    openRemarkModal = () => {
        this.closeModal();
        this.setState({
            remarkModal: true
        });
    };

    //  resolve remarking detail
    resolveRemarkModal = () => {
        this.closeModal();
        this.resolveConfirmModal();
    };

    //  get value from input
    getValue = (e) => {
        this.setState({
            id: e
        });
    };

    //  get array of details from database
    getInfo = () => {
        if(this.state.id.length){
            this.setState({
                inProgress: true
            });
            this.props.getEquipmentForUnit(this.state.id)
        }
    };

    //  action for marking detail, send request to server and mark detail
    mark = (id) => {
        this.props.markEquipment(id);
    };

    markerator = (data) => {
        this.props.markeratorRequest(data);
    };

    render() {

        const { items } = this.state;

        return(
            <SafeAreaView
                style={styles.container}
            >
                <_Modal
                    closeModal={this.closeModal}
                    visible={this.state.modal}
                    type={'alert'}
                >
                    {
                        `Номер ${this.state.id} не найден в базе!`
                    }
                </_Modal>
                <_Modal
                    closeModal={this.closeModal}
                    confirmModal={this.resolveConfirmModal}
                    visible={this.state.confirmModal}
                    type={'confirm'}
                >
                    {
                        `Вы уверены что хотите промаркировать оборудование`
                    }
                </_Modal>
                <_Modal
                    closeModal={this.openRemarkModal}
                    confirmModal={this.resolveConfirmMarkModal}
                    visible={this.state.confirmMarkModal}
                    type={'confirm'}
                >
                    Маркировка прошла успешно?
                </_Modal>
                <_Modal
                    closeModal={this.closeModal}
                    confirmModal={this.resolveRemarkModal}
                    visible={this.state.remarkModal}
                    type={'confirm'}
                >
                    Повторить маркировку?
                </_Modal>
                <Header>
                    Маркировка оборудования
                </Header>
                <View
                    style={styles.inputWithButton}
                >
                    <TextInput
                        value={this.state.id} //    todo remove value
                        onChangeText={this.getValue}
                        placeholder={'введите номер партии'}
                        style={styles.input}
                    />
                    <_Button
                        onClick={this.getInfo}
                    >
                        Отправить
                    </_Button>
                </View>
                {
                    this.state.inProgress ? <Spinner/>
                    :
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                        >
                            {
                                items ? items.map((item, i) => (
                                    !item.is_marked ?
                                    <TouchableOpacity
                                        onPress={() => this.openConfirmModal(item, i)}
                                        style={styles.item}
                                        key={i}
                                    >
                                        <Text
                                            style={styles.itemText}
                                        >
                                            { item.name }
                                        </Text>
                                    </TouchableOpacity> : null
                                )) : null
                            }
                        </ScrollView>
                }
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => ({
    error: state.error.errors.markingEquipmentError,
    marking: state.marking.markingEquipment
});

export default connect(mapStateToProps, { closeError, markEquipment, markeratorRequest, getEquipmentForUnit })(MarkingEquipment);
