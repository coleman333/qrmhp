import React, { Component } from 'react';
import {  SafeAreaView, View, TextInput, TouchableOpacity, Keyboard, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Button } from '../../Components/Button';
import Header from '../../Components/Header';
import Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import { searchAction, searchWarehouseSharpeningAction } from '../../../actions/searchAction';
import { getTime } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import styles from './styles';

class Identification extends Component {
  constructor (props) {
    super(props);
    this.state = {
      modal: false,
      serverError: false,
      id: '',
      connection: true,
      inProgress: false,
      connectionModal: false
    };
    this.openCamera = this.openCamera.bind(this);
    this.getInfo = this.getInfo.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.getValue = this.getValue.bind(this);
  }

  async componentWillMount() {
    const knifeTypeResult = await AsyncStorage.getItem( 'knifeType');
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      modal: nextProps.error,
      connection: nextProps.connection,
      inProgress: false,
      identifyWarehouseIdError: nextProps.identifyWarehouseIdError,
      identifySharpeningIdError: nextProps.identifySharpeningIdError,
    });
  }

  //  open camera to scan QR-code
  openCamera () {
    this.props.navigation.navigate('Qrscaner', { text: 'Отсканируйте QR-код', type: 'search', buttons: false, identificationFlag: true });
  }

  searchActionFunc = (id, type, buttons, navigation) =>{
    this.props.searchAction(id, type, buttons, navigation)
      .then(() => {
        this.setState({
          inProgress: false
        });
      });
  };

  searchWarehouseSharpeningActionFunc = (id, type, buttons, navigation) =>{
    this.props.searchWarehouseSharpeningAction(id, type, buttons, navigation)
      .then(() => {
        this.setState({
          inProgress: false
        });
      });
  };

  getRidOfSpecialElements(){
    let id = this.state.id.toLowerCase();
    return id.replace(/[&\/\\#,+()$~%. '":*?<>{}]/g, '');
  }

  //  get info about material or equipment according to id
  getInfo () {

    if (this.state.id.length) {
      if (this.state.connection) {
        let id = this.getRidOfSpecialElements();
        switch(id[0]){
          case 'e':{
            this.searchActionFunc(id, 'identify_id', false, this.props.navigation);
          }
            break;
          case 'i':{
            this.searchActionFunc(id, 'identify_id', false, this.props.navigation);
          }
            break;
          case 'w': {
            this.searchWarehouseSharpeningActionFunc(id, 'identify_warehouse_id', false, this.props.navigation);
          }
            break;
          case 'r': {
            this.searchWarehouseSharpeningActionFunc(id, 'identify_sharpening_id', false, this.props.navigation);
          }
          break;
          default: {
            this.setState({identifyWarehouseIdError: true})
          }
            break;
        }
      } else {
        this.setState({
          connectionModal: true
        });
      }
    }
  }

  //  close modal window
  closeModal () {
    this.setState({
      id: '',
      connectionModal: false,
      identifyWarehouseIdError: false,
      identifySharpeningIdError: false,
    });
    this.props.closeError();
  }

  //  get value from input
  getValue (e) {
    this.setState({
      id: e
    });
  }

  render () {
    return (
      <SafeAreaView
        style={styles.container}
      >
        <TouchableOpacity
          activeOpacity={1.0}
          onPress={Keyboard.dismiss}
          style={{ flex: 1 }}
        >
          <React.Fragment>
            <Modal
              closeModal={this.closeModal}
              visible={this.state.modal}
              type="alert"
            >
              {`Идентификатор ${this.state.id} не найден в базе!`}
            </Modal>
            <Modal
              closeModal={this.closeModal}
              visible={this.state.identifyWarehouseIdError || this.state.identifySharpeningIdError}
              type="alert"
            >
              {`Идентификатор ${this.state.id} не найден в базе!`}
            </Modal>
            <Modal
              closeModal={this.closeModal}
              visible={this.state.connectionModal}
              type="alert"
            >
              Отсутствует интернет соединение
            </Modal>
             <Header>
              Идентификация ТМЦ
            </Header>
            {
              this.state.inProgress
                ? <Spinner />
                : (
                  <React.Fragment>
                    <View
                      style={styles.item}
                    >
                      <View
                        style={styles.inputWithButton}
                      >
                        <TextInput
                          value={this.state.id}
                          onChangeText={this.getValue}
                          placeholder="введите идентификатор"
                          style={styles.input}
                        />
                        <Button
                          onClick={this.getInfo}
                        >
                          Отправить
                        </Button>
                      </View>
                    </View>
                    <View
                      style={styles.item}
                    >
                      <Button
                        onClick={this.openCamera}
                      >
                        Открыть камеру
                      </Button>
                    </View>
                  </React.Fragment>
                )
            }
          </React.Fragment>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.identifyIdError,
  serverError: state.error.errors.identifyServerError,
  connection: state.connection.connection,
  allMarkerators: state.inventory.allMarkerators,
  identifyWarehouseIdError: state.error.errors.identifyWarehouseIdError,
  identifySharpeningIdError: state.error.errors.identifySharpeningIdError,
});

export default connect(mapStateToProps, { closeError, searchAction, getTime, searchWarehouseSharpeningAction })(Identification);
