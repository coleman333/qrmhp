import React, { Component } from 'react';
import {
  SafeAreaView, View, TouchableOpacity, Keyboard, AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { Button } from '../../Components/Button';
import Header from '../../Components/Header';
import Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import { isAuth } from '../../../actions/AuthAction';
import { closeError } from '../../../actions/errorAction';
import styles from './styles';

class RegisterPage extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      modal: false,
      serverError: false,
      id: '',
      connection: true,
      inProgress: false,
      connectionModal: false,
      page: ''
    };
    this.openCamera = this.openCamera.bind( this );
    this.getInfo = this.getInfo.bind( this );
    this.closeModal = this.closeModal.bind( this );
    this.getValue = this.getValue.bind( this );
  }

  async componentWillMount(  ) {
   try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {
        this.props.navigation.navigate('mainNavigator');
      }
    } catch (error) {
      console.log(error);
    }
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      serverError: nextProps.serverError,
      connection: nextProps.connection,
      inProgress: false,
      checkWarehouseError: nextProps.checkWarehouseError,
      checkWarehouseResult: nextProps.checkWarehouseResult // if warehouse exist
    })

  }

  //  open camera to scan QR-code
  openCamera() {
    this.props.navigation.navigate('AuthCamera');
  }

  //  get info about material or equipment according to id
  getInfo() {
    let id = this.state.id;
    id = id.toLowerCase();

    if ( id.length ) {
      if ( this.state.connection ) {
        this.setState( {
          ...this.state,
          inProgress: true
        } );
        this.props.checkWarehouse( id )
          .then( () => {
            this.setState( {
              ...this.state,
              inProgress: false
            } );
            this.props.navigation.navigate( (this.state.page), {

              warehouse_id: this.state.id
            } )
          } );
      } else {
        this.setState( {
          ...this.state,
          connectionModal: true
        } );
      }
    }
  }

  //  close modal window
  closeModal() {
    this.setState( {
      id: '',
      connectionModal: false
    } );
    this.props.closeError();
  }

  //  get value from input
  getValue( e ) {
    this.setState( {
      id: e
    } );
  }

  render() {
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <TouchableOpacity
          activeOpacity={ 1.0 }
          onPress={ Keyboard.dismiss }
          style={ { flex: 1 } }
        >
          <React.Fragment>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.checkWarehouseError }
              type={ 'alert' }
            >
              Такого склада не существует
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.modal }
              type="alert"
            >
              { `Идентификатор ${ this.state.id } не найден в базе!` }
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.connectionModal }
              type="alert"
            >
              Отсутствует интернет соединение
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.serverError }
              type="alert"
            >
              Сервер не отвечает
            </Modal>
            <Header page={'auth'}>
              Авторизация
            </Header>
            {
              this.state.inProgress
                ? <Spinner/>
                : (
                  <React.Fragment>
                    <View style={ styles.item } >
                      <Button onClick={ this.openCamera } > Войти в систему </Button>
                    </View>
                  </React.Fragment>
                )
            }
          </React.Fragment>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.identifyIdError,
  serverError: state.error.errors.identifyServerError,
  connection: state.connection.connection,
  checkWarehouseError: state.error.errors.checkWarehouseError,
  checkWarehouseResult: state.inventory.checkWarehouseResult

});

export default connect( mapStateToProps, { isAuth, closeError } )( RegisterPage );
