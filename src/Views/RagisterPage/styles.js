import { StyleSheet } from 'react-native';

import colors from '../../Constants/Colors';
import { fontSize } from '../../Constants/Text';

export default StyleSheet.create({
  input: {
    flex: 5,
    height: 40,
    textAlign: 'center',
    paddingHorizontal: 20,
    fontSize: fontSize.small,
    color: colors.gray,
    justifyContent: 'center'
  },
  button: {
    flex: 3
  },
  inputWithButton: {
    borderColor: colors.grayBorder,
    borderWidth: 1,
    borderRadius: 20,
    height: 40,
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginBottom: 20,
    justifyContent: 'space-between'
  },
  item: {
    flex: 1,
    justifyContent: 'center'
  }
});
