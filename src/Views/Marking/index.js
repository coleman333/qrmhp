import React, { Component } from 'react';
import { Text, SafeAreaView, View, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import _Button from '../../Components/Button';
import _Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import styles from './styles';
import { getAllUnmarkedInventoryAction, markingAction, markDetail, markeratorRequest, getAllMarkerators } from '../../../actions/markingAction';
import { closeError } from '../../../actions/errorAction';

class Marking extends Component {

  state = {
    modal: false,
    confirmModal: false,
    confirmMarkModal: false,
    successModal: false,
    amount: 0,
    inProgress: false,
    remarkModal: false,
    nextModal: false,
    id: '',
    item: null,
    items: [],
    port: '',
    host: ''
  };

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      items: nextProps.marking,
      allUnmarkedInventory: nextProps.allUnmarkedInventory,
      inProgress: false
    } );
    if(this.state.allUnmarkedInventory===[] || this.state.allUnmarkedInventory === null){
      this.setState({noConnectionsWindow: true})
    }
  };

  async componentDidMount(){
    //get all unmarked details
    await this.props.getAllUnmarkedInventoryAction(this.state.id);
    await this.setState({allUnmarkedInventory: this.props.allUnmarkedInventory });
  }

  //  close modal windows
  closeModal = () => {
    this.props.closeError();
    this.setState( {
      confirmModal: false,
      nextModal: false,
      confirmMarkModal: false,
      remarkModal: false,
      successModal: false,
      noConnectionsWindow: false,
    } );
  };

  //  open modal window to confirm marking details
  openConfirmModal = ( item ) => {
    this.setState( {
      confirmModal: true,
      item: item,
    } );
  };

  resolveConfirmModal = async () => {

    const { connections: [device] } = await this.props.getAllMarkerators();

    if (device) {
      this.setState( {
        inProgress: false
      } );
      this.closeModal();
      this.props.markDetail(device.id, this.state.item._id).then(() => {
        this.openConfirmMarkModal();
      });
    } else {
      this.closeModal();
      this.setState({noConnectionsWindow: true });
    }
  };

  //  open modal to confirm success marking
  openConfirmMarkModal = () => {
    this.setState( {
      confirmMarkModal: true
    } );
  };

  resolveConfirmMarkModal = async () => {
    this.closeModal();
    // this.openNextModal();
    this.getInfo();
    this.setState( {
      successModal: true
    });
  };

  //  open modal window for next mark
  openNextModal = () => {
    this.setState( ( prevState ) => {
      return {
        amount: --prevState.amount
      }
    }, () => {
      if ( this.state.amount > 0 ) {
        this.setState( {
          nextModal: true
        } )
      } else {
        this.setState( {
          successModal: true
        } );
      }
    } );
  };

  //  resolve next mark
  resolveNextModal = () => {
    this.closeModal();
    this.resolveConfirmModal();
  };

  //  open modal window if marking not success and try remark detail
  openRemarkModal = () => {
    this.closeModal();
    this.setState( {
      remarkModal: true
    } );
  };

  //  resolve remarking detail
  resolveRemarkModal = () => {
    this.closeModal();
    this.resolveConfirmModal();
  };

  //  get value from input
  getValue = ( e ) => {
    if(e===''||e===undefined||e===null){
      e=''
    }
    this.setState( {
      id: e
    } );
  };

  //  get array of details from database
  getInfo = () => {
      this.props.getAllUnmarkedInventoryAction(this.state.id )
  };

  //  action for marking detail, send request to server and mark detail
  mark = async ( id ) => {
    this.props.markDetail( id );
    await this.props.getAllUnmarkedInventoryAction();
    await this.setState({allUnmarkedInventory: this.props.allUnmarkedInventory });  };

  markerator = ( data ) => {
    this.props.markeratorRequest( data );
  };

  render() {
    const { items, amount } = this.state;

    return (
      <SafeAreaView
        style={ styles.container }
      >
        <_Modal
          closeModal={ this.closeModal }
          visible={ this.state.successModal }
          type={ 'success' }
        >
          деталь успешно промаркирована!
        </_Modal>

        <_Modal
          closeModal={ this.closeModal }
          confirmModal={ this.resolveConfirmModal }
          visible={ this.state.confirmModal }
          type={ 'confirm' }
        >
          {
            `Вы уверены что хотите промаркировать деталь ${ this.state.item ? this.state.item._id : null } `
          }
        </_Modal>
        <_Modal
          closeModal={ this.openRemarkModal }
          confirmModal={ this.resolveConfirmMarkModal }
          visible={ this.state.confirmMarkModal }
          type={ 'confirm' }
        >
          Маркировка прошла успешно?
        </_Modal>
        <_Modal
          closeModal={ this.closeModal }
          confirmModal={ this.resolveNextModal }
          visible={ this.state.nextModal }
          type={ 'confirm' }
        >
          {
            `Хотите продолжить?\nИдентификатор детали ${ this.state.item ? this.state.item._id : null }`
          }
        </_Modal>
        <_Modal
          closeModal={ this.closeModal }
          confirmModal={ this.resolveRemarkModal }
          visible={ this.state.remarkModal }
          type={ 'confirm' }
        >
          Повторить маркировку?
        </_Modal>

        <_Modal
          closeModal={ this.closeModal }
          visible={ this.state.noConnectionsWindow }
          type={ 'alert' }
        >
          нет соединений с удаленным маркиратором
        </_Modal>

        <Header>
          Маркировка ТМЦ
        </Header>
        <View
          style={ styles.inputWithButton }
        >
          <TextInput
            onChangeText={ this.getValue }
            placeholder={ 'введите номер партии' }
            style={ styles.input }
          />
          <_Button
            onClick={ this.getInfo }
          >
            Отправить
          </_Button>
        </View>
        {
          this.state.inProgress ? <Spinner/> :
            <ScrollView showsVerticalScrollIndicator={ false } >
              {
                this.state.allUnmarkedInventory ?
                  this.state.allUnmarkedInventory.map( ( item, i ) => (
                  // item.amount ?
                    <TouchableOpacity
                      onPress={ () => this.openConfirmModal( item, i ) }
                      style={ styles.item }
                      key={ i }
                    >
                      <Text style={ styles.itemText } > { item.title } </Text>
                    </TouchableOpacity>
                    // : null
                ) )
                  : null
              }
            </ScrollView>
        }
      </SafeAreaView>
    )
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.markingError,
  marking: state.marking.marking,
  allUnmarkedInventory: state.marking.allUnmarkedInventory
});

export default connect( mapStateToProps, { getAllMarkerators, getAllUnmarkedInventoryAction,
  markingAction, closeError, markDetail, markeratorRequest } )( Marking );
