import { StyleSheet } from 'react-native';
import Colors from '../../Constants/Colors';
import Others from '../../Constants/Other';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginBottom: 20
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    marginVertical: 5,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  wrapper: {
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.grayBorder,
    minHeight: 50,
    borderRadius: Others.borderRadius,
    marginVertical: 10
  },

  equipmentTitleContainer:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  equipLeftPart:{
    width: '40%',
    textAlign: 'left'
  },
  equipRightPart:{
    width: '50%',
    textAlign: 'right'
  },

  inventoryItem: {

    fontFamily: 'TT Norms ExtraBold',
    borderWidth: 1,
    borderColor: Colors.grayBorder,
    borderRadius: Others.borderRadius,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    margin: 10,
    padding: 10
  },
  serveDateContainer:{
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
