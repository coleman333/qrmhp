import React, { Component } from 'react';
import {
  SafeAreaView, View, Text, ScrollView, TouchableOpacity, Linking
} from 'react-native';
import { connect } from 'react-redux';
import { getManualUrl } from '../../../actions/equipmentAction';
import Spinner from '../../Components/ActivityIndicator';

import PropTypes from 'prop-types';
import styles from './styles';
import Header from '../../Components/Header';
import { Button } from '../../Components/Button';
import Modal from '../../Components/Modal';


const messageMount = 'Отсканируйте QR-код монтируемой детали';
const messageUnmount = 'Отсканируйте QR-код демонтируемой детали';

class EquipmentDetails extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      details: false,
      modal: false,
      mount: false,
      unmount: false,
      inProgress: false,
    };
    this.toggleDetails = this.toggleDetails.bind( this );
    this.refusedModal = this.refusedModal.bind( this );
    this.confirmModal = this.confirmModal.bind( this );
    this.mount = this.mount.bind( this );
    this.unmount = this.unmount.bind( this );
    this.openCamera = this.openCamera.bind( this );
  }

  async componentDidMount() {
    this.setState({inProgress:false})
  }

  //  show or close additional information
  toggleDetails() {
    this.setState( prevState => ({
      details: !prevState.details
    }) );
  }

  refusedModal() {
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
  }

  confirmModal() {
    const { mount } = this.state;
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
    mount
      ? this.openCamera( messageMount, 'mountInventory' )
      : this.openCamera( messageUnmount, 'unmountInventory' );
  }

  mount() {
    this.setState( {
      modal: true,
      mount: true
    } );
  }

  unmount( id, name ) {
    this.inventoryId = id;
    this.unmountInventory = name;
    this.setState( {
      modal: true,
      unmount: true
    } );
  }

  openCamera( text, type ) {
    const { navigation, equipment } = this.props;
    navigation.navigate( 'Qrscaner', {
      text, type, equipment_id: equipment.id, inventoryId: this.inventoryId
    } );
  }

  redirectToManualUrl() {
    const { manualUrl } = this.props;
    Linking.openURL( manualUrl.data )
  }

  redirectToManualLinksPromptsUrlsPage(){
    this.props.navigation.navigate('LinksOnVideoPrompts', {id: this.props.equipment.id, title: this.props.equipment.title});

  }

  render() {
    const { equipment, navigation, status } = this.props;
    const inventories = equipment.inventories;
    const name = equipment.title;
    const { modal, mount, unmount, details } = this.state;
    const buttons = navigation.getParam( 'buttons' );
    const detail = (
      <View>
        {
          inventories.map( ( item, index ) => (
            <View
              style={ styles.inventoryItem }
              key={ item }
            >
              <View style={ styles.serveDateContainer }>
                <Text style={ { width: '45%' } }>{ 'установлено: ' } </Text>
                <Text style={ { width: '55%', textAlign: 'right' } }>{ item.title } </Text>
              </View>
              <View style={ styles.serveDateContainer }>
                <Text style={ { width: '45%' } }>{ 'ID: ' } </Text>
                <Text style={ { width: '55%', textAlign: 'right' } }>{ item.id } </Text>
              </View>
              <View style={ styles.serveDateContainer }>
                <Text style={ { width: '45%' } }>{ 'Наработка: ' } </Text>
                <Text style={ { width: '55%', textAlign: 'right' } }>{ `days: ${ item.convert_time.days } hours: ${ item.convert_time.hours } minutes: ${ item.convert_time.minutes }` } </Text>
              </View>
              {
                buttons
                  ? (
                    <Button
                      onClick={ () => this.unmount( item.id, item.title ) }
                    >
                      Демонтровать
                    </Button>
                  )
                  : null
              }
            </View>
          ) )
        }
      </View>
    );

    return (
      <SafeAreaView
        style={ styles.container }
      >
        <Header>
          {
            buttons ? `Монтаж/Демонтаж` : `Информация об оборудовании`
          }
        </Header>
        <Modal
          closeModal={ this.refusedModal }
          confirmModal={ this.confirmModal }
          visible={ modal }
          type="confirm"
        >
          {
            mount ? `Вы уверены что хотите монтировать новую деталь на ${ name }`
              : unmount ? `Вы уверены что хотите демонтировать ${ this.unmountInventory } с ${ name }` : null
          }
        </Modal>
        { this.state.inProgress ? <Spinner/> :

          <ScrollView>
            <TouchableOpacity
              onPress={ this.toggleDetails }
              style={ styles.wrapper }
            >
              {/*<View style={ styles.item } >*/ }
              <View style={ styles.equipmentTitleContainer }>
                <Text style={ styles.equipLeftPart }> Наименование </Text>
                <Text style={ styles.equipLeftPart }> { name } </Text>
              </View>
              {
                details ? detail : null
              }
            </TouchableOpacity>
            {
              buttons
                ? (
                  <View>
                    <View style={ { marginBottom: '1%' } }>
                      <Button onClick={ () => this.mount() }> Монтировать новую деталь </Button>
                    </View>
                  </View>
                )
                : null
            }
            { this.props.navigation.getParam( 'type' ) === 'identify_id' &&
            <Button style={ { marginTop: '1%' } }
                    onClick={ this.redirectToManualLinksPromptsUrlsPage.bind( this ) }>
              Список видео
            </Button>
            }
          </ScrollView>
        }
      </SafeAreaView>
    );
  }
}

EquipmentDetails.defaultProps = {
  equipment: {},
  navigation: {}
};

EquipmentDetails.propTypes = {
  equipment: PropTypes.instanceOf( Object ),
  navigation: PropTypes.instanceOf( Object )
};

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  manualUrl: state.equipment.manualUrl
});

export default connect( mapStateToProps, { getManualUrl } )( EquipmentDetails );
