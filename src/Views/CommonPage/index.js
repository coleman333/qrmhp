import React, { Component } from 'react';
import {
  SafeAreaView, View, TextInput, TouchableOpacity, Keyboard, AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';

import { Button } from '../../Components/Button';
import Header from '../../Components/Header';
import Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import { checkWarehouse, getTime } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';

import styles from './styles';

class CommonPage extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      modal: false,
      serverError: false,
      id: '',
      connection: true,
      inProgress: false,
      connectionModal: false,
      page: ''
    };
    this.openCamera = this.openCamera.bind( this );
    this.getInfo = this.getInfo.bind( this );
    this.closeModal = this.closeModal.bind( this );
    this.getValue = this.getValue.bind( this );
  }

  componentDidMount() {
    if(this.props.navigation.state.routeName === 'CommonPageWarehouse'){
      this.setState({page: 'Stock'})
      this.setState({test: 'test'})
    } else {
      this.setState({page: 'Sharpening'})
    }
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      connection: nextProps.connection,
      inProgress: false,
      checkWarehouseError: nextProps.checkWarehouseError,
      checkWarehouseResult: nextProps.checkWarehouseResult // if warehouse exist
    });
    if ( nextProps.checkWarehouseResult
      // && nextProps.checkWarehouseResult.status !== 404
    ) { }
  }

  //  open camera to scan QR-code
  openCamera() {
    let page = (this.props.navigation.state.routeName === 'CommonPageWarehouse') ? 'Stock' : 'Sharpening';
    this.props.navigation.navigate( page
      , {
      id_warehouse: this.state.id
      }
    );
  }

  //  get info about material or equipment according to id
  getInfo() {
    let id = this.state.id;
    id = id.toLowerCase();
    // id = id.replace(" ", '');
    id =id.trim();

    if ( id.length ) {
      if ( this.state.connection ) {
        this.setState( {
          ...this.state,
          inProgress: true
        } );
        switch ( id[0] ) {
          case 'w':
          {
            if(this.props.navigation.state.routeName === 'CommonPageWarehouse'){
              this.props.checkWarehouse( id )
                .then( () => {
                  this.setState( {
                    ...this.state,
                    inProgress: false
                  } );
                  this.props.navigation.navigate( (this.state.page), {
                    warehouse_id: this.state.id
                  } )
                } );
            }else{
              this.setState({checkWarehouseError: true});
            }
          }
            break;
          case 'r':
          {
            if(this.props.navigation.state.routeName === 'CommonPageSharpening'){
              this.props.checkWarehouse( id )
                .then( () => {
                  this.setState( {
                    ...this.state,
                    inProgress: false
                  } );
                  this.props.navigation.navigate( (this.state.page), {
                    warehouse_id: this.state.id
                  } )
                } );
            }else{
              this.setState({checkWarehouseError: true});
            }
          }
            break;
          default:{
            this.setState({checkWarehouseError: true});
          }
            break;
        }
      } else {
        this.setState( {
          ...this.state,
          connectionModal: true
        } );
      }
    }
  }

  //  close modal window
  closeModal() {
    this.setState( {
      id: '',
      connectionModal: false,
      checkWarehouseError:false,
      inProgress: false
    } );
    this.props.closeError();
  }

  //  get value from input
  getValue( e ) {
    this.setState( {
      id: e.trim()
    } );
  }

  render() {
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <TouchableOpacity
          activeOpacity={ 1.0 }
          onPress={ Keyboard.dismiss }
          style={ { flex: 1 } }
        >
          <React.Fragment>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.checkWarehouseError }
              type={ 'alert' }
            >
              { this.state.page && this.state.page !== 'Sharpening' ?
                'Такого склада не существует' :
                'Такого участка заточки не существует'
              }
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.modal }
              type="alert"
            >
              { `Идентификатор ${ this.state.id } не найден в базе!` }
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.state.connectionModal }
              type="alert"
            >
              Отсутствует интернет соединение
            </Modal>
             <Header>
              { this.props.navigation.state.routeName === 'CommonPageWarehouse' ? 'Принять На Склад' : 'Принять на заточку' }
            </Header>
            {
              this.state.inProgress
                ? <Spinner/>
                : (
                  <React.Fragment>
                    <View
                      style={ styles.item }
                    >
                      <View
                        style={ styles.inputWithButton }
                      >
                        <TextInput
                          value={ this.state.id }
                          onChangeText={ this.getValue }
                          placeholder={ this.props.navigation.state.routeName === 'CommonPageWarehouse' ? 'введите идентификатор' : 'введите идентификатор' }
                          style={ styles.input }
                        />
                        <Button
                          onClick={ this.getInfo }
                        >
                          Отправить
                        </Button>
                      </View>
                    </View>
                    <View
                      style={ styles.item }
                    >
                      <Button
                        onClick={ this.openCamera }
                      >
                        Открыть камеру
                      </Button>
                    </View>
                  </React.Fragment>
                )
            }
          </React.Fragment>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.identifyIdError,
  serverError: state.error.errors.identifyServerError,
  connection: state.connection.connection,
  checkWarehouseError: state.error.errors.checkWarehouseError,
  checkWarehouseResult: state.inventory.checkWarehouseResult

});

export default connect( mapStateToProps, { checkWarehouse, closeError, getTime } )( CommonPage );
