import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, } from 'react-native';
import { RNCamera as Camera} from 'react-native-camera';//  todo: maybe change library to new
import { connect } from 'react-redux';
import styles from './styles';
import ViewFinder from './ViewFinder';
import _Modal from '../../Components/Modal';
import { writeOff } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import Spinner from '../../Components/ActivityIndicator';

export class WriteOffCamera extends Component {

  state = {
    stock: true,
    stockId: null,
    nextWindow: false,
    stockError: false,
    bindError: false,
    successModal: false,
    scan: true,
    mac: null,
    writeOffConfirmationWindow: false
  };

  componentDidMount() {
    const pickerArray = ['Поломка 1', 'Поломка 2', 'Поломка 3'];
    this.setState({pickerArray});

  }

  componentWillReceiveProps( nextProps ) {
      this.setState( {
        writeOffAlreadyWrittenOffError: nextProps.writeOffAlreadyWrittenOffError,
        writeOffInventoryNotExists: nextProps.writeOffInventoryNotExists,
      } );
  };


  getInventoryInfo(id) {
    id = id.toLowerCase();
    this.setState( { confirmDeletingModal: true, id: id, } );
  }

  confirmWritingOffModal() {
    this.setState( { confirmDeletingModal: false,
      nameModal: true
    } );
  }

  chooseBreakdown( breakdown ) {
    const metadata = { description: breakdown };
    this.setState( { metadata } );
  }

  sendWritingOffRequest(){
    this.setState( { nameModal: false});
    if(this.state.metadata === undefined){
      const metadata = { description: this.state.pickerArray[0] };
      this.props.writeOff( this.state.id, { metadata: metadata} )
        .then( () => {
          this.setState( {
            ...this.state,
            inProgress: false,
            writeOffConfirmationWindow: true
          } );
        } );
    }else{
      this.props.writeOff( this.state.id, { metadata: this.state.metadata} )
        .then( () => {
          this.setState( {
            ...this.state,
            inProgress: false,
            writeOffConfirmationWindow: true
          } );
        } );
    }
  }

  resolveSuccessModal = () => {
    this.setState( {
      successModal: false,
      scan: false
    } );
    this.props.navigation.navigate('mainNavigator')

  };

  openNextWindow = () => {
    this.setState( {
      nextWindow: true,
      scan: false
    } );
  };

  resolveNextWindow = () => {
    this.setState( {
      nextWindow: false,
      scan: true
    } );
  };

  rejectNextWindow = () => {
    // this.goBack();
    this.props.navigation.navigate( 'Identification' );
    this.setState( {
      nextWindow: false,
      scan: true,
      stock: true
    } );
  };

  closeModal = () => {
    this.setState( {
      scan: true,          //true
      confirmDeletingModal: false,
    } );
    this.props.closeError();
  };

  turnFlash = () => {
    if ( this.state.torch === Camera.Constants.FlashMode.torch ) {
      this.setState( { torch: Camera.Constants.FlashMode.off, flash: Camera.Constants.FlashMode.off } )
    } else {
      this.setState( { torch: Camera.Constants.FlashMode.torch, flash: Camera.Constants.FlashMode.torch } )
    }
  };



  render() {
    let { confirmDeletingModal, nameModal, pickerArray } = this.state;

    return (
      <View
        style={ { flex: 1 } }
      >
        <_Modal
          closeModal={ this.closeModal }
          confirmModal={ this.confirmWritingOffModal.bind(this) }
          visible={ confirmDeletingModal }
          type="confirm"
        >
          { `Вы действительно хотите списать с производства ${ this.state.id }` }
        </_Modal>
        <_Modal
          closeModal={ this.closeModal }
          visible={ this.props.writeOffAlreadyWrittenOffError }
          type={ 'alert' }
        >
          Данное ТМЦ уже ранее было списано с производства
        </_Modal>

        <_Modal
          closeModal={ this.closeModal }
          visible={ this.props.writeOffInventoryNotExists }
          type={ 'alert' }
        >
          Такого ТМЦ не существует
        </_Modal>

        <_Modal
          pickerArray = { pickerArray }
          onChange={ breakdown => this.chooseBreakdown( breakdown ) }
          closeModal={ () => this.sendWritingOffRequest( 'breakdown' ) }
          visible={ nameModal }
          type="picker"
        />
        <_Modal
          closeModal={ ()=>{
            this.setState({writeOffConfirmationWindow: false} );this.props.navigation.navigate('WritingOffPage')
          } }
          visible={ this.state.writeOffConfirmationWindow }
          type={ 'success' }
        >
          Списание ТМЦ прошло успешно
        </_Modal>
        { this.state.inProgress ? <Spinner/> :

          <Camera
            flashMode={ this.state.torch }
            style={ { flex: 1 } }
            onBarCodeRead={ ( { data } ) => {
              if ( this.state.scan ) {
                this.setState( { scan: false } );
                this.getInventoryInfo( data );
              }
            } }
          >
            { Platform.OS === 'ios' && <ViewFinder/> }

            <React.Fragment>
              <View style={ styles.lightButton }>
                <TouchableOpacity onPress={ this.turnFlash } style={ styles.stockButton }>
                  <Text style={ styles.stockButtonStyle }> освещение </Text>
                </TouchableOpacity>
              </View>
              { Platform.OS === 'android' && <ViewFinder/> }
              <View style={ styles.header }>
                <Text style={ styles.headerText }> Отсканируйте QR-код для списания ТМЦ </Text>
              </View>
            </React.Fragment>
          </Camera>
        }
      </View>
    )
  }
}

const mapStateToProps = ( state ) => ({
  writeOffAlreadyWrittenOffError: state.error.errors.writeOffAlreadyWrittenOffError,
  writeOffInventoryNotExists: state.error.errors.writeOffInventoryNotExists,
  token: state.auth.token,
});

export default connect( mapStateToProps, { closeError, writeOff } )( WriteOffCamera )
