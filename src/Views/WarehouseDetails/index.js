import React, { Component } from 'react';
import {
  SafeAreaView, View, Text, ScrollView, TouchableOpacity, Linking
} from 'react-native';
import { connect } from 'react-redux';
import { getManualUrl } from '../../../actions/equipmentAction';
import styles from './styles';
import Header from '../../Components/Header';
import { Button } from '../../Components/Button';
import Modal from '../../Components/Modal';

const messageMount = 'Отсканируйте QR-код монтируемой детали';
const messageUnmount = 'Отсканируйте QR-код демонтируемой детали';

class WarehouseDetails extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      details: false,
      modal: false,
      mount: false,
      unmount: false
    };
    this.toggleDetails = this.toggleDetails.bind( this );
    this.refusedModal = this.refusedModal.bind( this );
    this.confirmModal = this.confirmModal.bind( this );
    this.mount = this.mount.bind( this );
    this.unmount = this.unmount.bind( this );
    this.openCamera = this.openCamera.bind( this );
  }

  async componentDidMount() {
    await this.props.getManualUrl()
  }

  //  show or close additional information
  toggleDetails() {
    this.setState( prevState => ({
      details: !prevState.details
    }) );
  }

  refusedModal() {
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
  }

  componentWillReceiveProps( nextProps ){
    this.setState({ warehouses:nextProps })
  }

  confirmModal() {
    const { mount } = this.state;
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
    mount
      ? this.openCamera( messageMount, 'mountInventory' )
      : this.openCamera( messageUnmount, 'unmountInventory' );
  }

  mount() {
    this.setState( {
      modal: true,
      mount: true
    } );
  }

  unmount( id, name ) {
    this.inventoryId = id;
    this.unmountInventory = name;
    this.setState( {
      modal: true,
      unmount: true
    } );
  }

  openCamera( text, type ) {
    const { navigation, equipment } = this.props;
    navigation.navigate( 'Qrscaner', {
      text, type, equipment_id: equipment.id, inventoryId: this.inventoryId
    } );
  }

  toggleGetInventoryDetails(itemTitle){
    this.props.navigation.navigate('InventoriesFromWarehouse',{id: this.props.navigation.getParam( 'id' ),itemTitle});
  }

  redirectToManualUrl() {
    const { manualUrl } = this.props;
    Linking.openURL( manualUrl.data )
  }

  render() {

    const { warehouses, } = this.props;
    const { details, } = this.state;
    const buttons = this.props.navigation.getParam( 'buttons' );
    const warehouseId = this.props.navigation.getParam( 'id' );
    const detail = (
      <View>
        {warehouses &&
          warehouses.map( ( item, index ) => (
            <TouchableOpacity
              onPress={ this.toggleGetInventoryDetails.bind(this,item.title) }
              key={index}
            >
            <View
              style={ styles.inventoryItem }
              key={ item }
            >
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'установлено: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ item.title } </Text>
                </View>
              <View style={ styles.serveDateContainer }>
                <Text style={ { width: '45%' } }>{ 'ID: ' } </Text>
                <Text style={ { width: '55%', textAlign: 'right' } }>{ item._id } </Text>
              </View>
              <View style={ styles.serveDateContainer }>
                <Text style={ { width: '45%' } }>{ 'count: ' } </Text>
                <Text style={ { width: '55%', textAlign: 'right' } }>{ item.count } </Text>
              </View>
              {
                buttons
                  ? (
                    <Button
                      onClick={ () => this.unmount( item.id, item.title ) }
                    >
                      Демонтровать
                    </Button>
                  )
                  : null
              }
            </View>
            </TouchableOpacity>
          ) )
        }
      </View>
    );

    return (
      <SafeAreaView
        style={ styles.container }
      >
        <Header>
          {
            warehouseId[0] ==='r' ? `Участок заточки` : `Склад`
          }
        </Header>
        <Modal
          closeModal={ this.refusedModal }
          confirmModal={ this.confirmModal }
          type="confirm"
        >
          {
            // mount ? `Вы уверены что хотите монтировать новую деталь на ${ name }`
            //   : unmount ? `Вы уверены что хотите демонтировать ${ this.unmountInventory } с ${ name }` : null
          }
        </Modal>
        <ScrollView>
          <TouchableOpacity
            onPress={ this.toggleDetails }
            style={ styles.wrapper }
          >
            <View style={ styles.equipmentTitleContainer }>
              <Text style={ styles.equipLeftPart }> Наименование </Text>
              <Text style={ styles.equipLeftPart }> { warehouseId } </Text>
            </View>

          </TouchableOpacity>
          {
            details ? detail : null
          }
          {
            buttons
              ? (
                <View>
                  <View style={ { marginTop: '1%' } }>
                    <Button onClick={ () =>
                      this.mount()
                    }> Монтировать новую деталь </Button>
                  </View>
                </View>
              )
              : null
          }
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  manualUrl: state.equipment.manualUrl,
  warehouses: state.inventory.warehouses,
});

export default connect( mapStateToProps, { getManualUrl } )( WarehouseDetails );
