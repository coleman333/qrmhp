import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, FlatList, Linking } from 'react-native';
import { connect } from 'react-redux';
import { equipmentAction } from '../../../actions/equipmentAction';
import { getPromptsLinks } from '../../../actions/searchAction';
import { getHistory } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import Header from '../../Components/Header';
import styles from './styles';
import Spinner from '../../Components/ActivityIndicator';

class LinksOnVideoPrompts extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      limit: 10,
      offset: 0,
      inProgress: false
    };
  }

  async componentWillMount() {

    await this.setState( {
      id: this.props.navigation.state.params,
      title: this.props.navigation.getParam('title'),
    } );
    this.props.getPromptsLinks( this.state.id.id, 'getPromptsLinksType', this.state.limit, this.state.offset )
      .then(()=>{
        this.setState({ inProgress: true });
      });
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      inProgress: false
    } );
    if ( nextProps.promptsLinks !== null ) {
      this.setState({ inProgress: false });
      this.setState( { promptsLinks: nextProps.promptsLinks.manuals, count: nextProps.promptsLinks.count, } )
    }
  };

  getRidOfSpecialElements(){
    let id = this.state.id.toLowerCase();
    return id.replace(/[&\/\\#,+()$~%. '":*?<>{}]/g, '');
  }

  redirectManualUrlVideo(video_url){
    Linking.openURL( video_url );
  }

  async onEndFlatListReached() {
    if ( this.state.count >= this.state.promptsLinks.length ) {
      await this.props.getPromptsLinks( this.state.id.id, 'getPromptsLinksType', this.state.limit += 10, this.state.offset = 0 )
    }
  }

  render() {
    let promptsLinks = this.state.promptsLinks;
    return (
      <SafeAreaView
        style={ styles.container }
      >
        {this.state.id.id[0] === 'i' &&<Header page={'history'}> { this.state.title } </Header>}
        {this.state.id.id[0] === 'e' &&<Header page={'history'}> { this.state.title } </Header>}
        { this.state.inProgress ? <Spinner/> :

          <View>
            <FlatList
              data={ promptsLinks }
              onEndReached={ this.onEndFlatListReached.bind( this ) }
              onEndThreshold={ 0.9 }
              keyExtractor={ ( item, index ) => index.toString() }
              contentContainerStyle={ { paddingBottom: 50 } }
              renderItem={ ( { item } ) =>
                <TouchableOpacity onPress={ this.redirectManualUrlVideo.bind( this, item.video_url ) }>
                  <View style={ styles.inventoryItem }>
                    <View style={ styles.serveDateContainer }>
                      { this.state.id.id[0] === 'e' &&
                      <React.Fragment>
                        <Text style={ { width: '45%' } }>{ 'ТМЦ: ' } </Text>
                        <Text style={ { width: '55%', textAlign: 'right' } }>{ item.inventory_title } </Text>
                      </React.Fragment>
                      }
                      { this.state.id.id[0] === 'i' &&
                      <React.Fragment>
                        <Text style={ { width: '45%' } }>{ 'Оборудование: ' } </Text>
                        <Text style={ { width: '55%', textAlign: 'right' } }>{ item.equipment_title } </Text>
                      </React.Fragment>
                      }
                    </View>
                  </View>
                </TouchableOpacity>
              }
            />
          </View>
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  promptsLinks: state.inventory.promptsLinks.data,
});

export default connect( mapStateToProps, { equipmentAction, closeError, getHistory, getPromptsLinks } )(
  LinksOnVideoPrompts );
