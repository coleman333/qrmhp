import React, { Component } from 'react';
import {
  SafeAreaView, View, Text, FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { equipmentAction } from '../../../actions/equipmentAction';
import { getMountingPrompts } from '../../../actions/searchAction';

import { getHistory } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import Header from '../../Components/Header';
import styles from './styles';

class MountingPromptsPage extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      limit: 10,
      offset: 0
    };

  }

  async componentWillMount() {
    await this.setState( { id: this.props.navigation.state.params } );
    await this.setState({title: this.props.navigation.getParam('title')} );
    await this.props.getMountingPrompts( this.state.id.id, this.state.limit, this.state.offset );
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      inventory_id: nextProps.navigation.state.params.id,
      inProgress: false
    } );
    if ( nextProps.history !== null ) {
      this.setState( { history: nextProps.history.data.transactions, count: nextProps.history.data.count, } )
    }
  };

  async onEndFlatListReached() {
    if ( this.state.count[0].count >= this.state.history.length ) {
      await this.props.getHistory( this.state.id, this.state.limit += 10, this.state.offset = 0 )
    }
  }

  render() {
    let mountingPrompts = this.props.mountingPrompts;
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <Header page={'history'} headerType={'MountingPromptsPage'} title={this.state.title}>
               Варианты монтажа
         </Header>

        <View>
          <FlatList
            data={ mountingPrompts }
            onEndReached={ this.onEndFlatListReached.bind( this ) }
            onEndThreshold={0.9}
            keyExtractor={ ( item, index ) => index.toString() }
            contentContainerStyle={ { paddingBottom: 50 } }
            renderItem={ ( { item } ) =>
              <View style={ styles.inventoryItem }>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'ID: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ item._id } </Text>
                </View>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Оборудование: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ item.title } </Text>
                </View>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Смонтированно: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ item.count } </Text>
                </View>

              </View>
            }
          />
        </View>

      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  history: state.inventory.history,
  mountingPrompts: state.inventory.mountingPrompts,

});

export default connect( mapStateToProps, { equipmentAction, closeError, getHistory, getMountingPrompts } )(
  MountingPromptsPage );
