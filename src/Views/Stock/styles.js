import { StyleSheet } from 'react-native';
import Colors from "../../Constants/Colors";

export default StyleSheet.create( {
  header: {
    position: 'absolute',
    zIndex: 1,
    width: '100%'
  },
  button: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    zIndex:2000
  },
  buttonContainer:{
    paddingBottom: '1%',
    width: '60%',
  },

  lightButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
  },
  manualEnterButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 60,
  },
  stockButton: {
    paddingHorizontal: 10,
    backgroundColor: Colors.mhp_blue,
    height: 38,
    borderRadius: 20,
    justifyContent: 'center',
    zIndex: 2002,
    width: '60%'
  },
  stockButtonStyle: {
    textAlign: 'center',
    color: Colors.white,
    zIndex: 2004,
  },
  headerText: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    padding: 30,
    margin: 'auto'
  }
} );
