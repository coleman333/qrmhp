import React, { Component } from 'react';
import { View, Text, Platform, } from 'react-native';
import { RNCamera as Camera} from 'react-native-camera';//  todo: maybe change library to new
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import styles from './styles';
import ViewFinder from './ViewFinder';
import _Modal from '../../Components/Modal';
import Button from '../../Components/Button';
import {
  bindInventoryToWarehouse,
  bindInventoryToSharpening,
  checkWarehouse,
  checkIfMount
} from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import { searchAction } from '../../../actions/searchAction';
import Spinner from '../../Components/ActivityIndicator';


export class Stock extends Component {

  state = {
    stock: true,
    stockId: null,
    nextWindow: false,
    stockError: false,
    bindError: false,
    successModal: false,
    scan: true,
    mac: null,
    enableButtonLocal: true,
  };


  componentDidMount() {
    DeviceInfo.getMACAddress().then( mac => {
      this.setState( {
        mac: mac
      } );
    } );
    this.setState( { page: this.props.navigation.state.routeName } );
    if ( this.props.navigation.state.params.warehouse_id ) {
      this.setState( { stock: false, stockId: this.props.navigation.state.params.warehouse_id }, () => {
      } )
    }
  }

  componentWillUnmount() {
    this.setState( {
      stock: true,
      scan: true,
      page: ''
    } );
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      bindError: nextProps.bindError,
      checkWarehouseError: nextProps.checkWarehouseError,
      checkWarehouseResult: nextProps.checkWarehouseResult,// if warehouse exist
      ifMount: nextProps.ifMount,
      modal: nextProps.error,
    } );
    if( nextProps.inventory && nextProps.inventory.is_ban){
      this.setState({is_ban: nextProps.inventory.is_ban})
    }
    if( nextProps.bindErrorNotFound ){
      this.setState({bindErrorNotFound: nextProps.bindErrorNotFound})
    }
  };

  getStock = async ( id ) => {
    if(id!== ''){
      id = id.toLowerCase();
    }else{
      return null
    }

    this.setState( {
      stockId: id,
      inProgress: true,
    } );
    switch ( id[0] ) {
      case 'w':
      {
        if(this.props.navigation.state.routeName === 'Stock'){
          await this.props.checkWarehouse( id );
          this.setState({successModal: true})
        }else{
          this.setState({checkWarehouseError: true});
        }
      }
      break;
      case 'r':
        {
          if(this.props.navigation.state.routeName === 'Sharpening'){
            await this.props.checkWarehouse( id );
            this.setState({successModal: true})
          }else{
            this.setState({checkWarehouseError: true});
          }
        }
        break;
      default:{
        this.setState({checkWarehouseError: true});
      }
      break;
    }
    this.setState( {

      scan: false,        //true
      stock: false,
      inProgress: false,
    } );
  };

  resolveSuccessModal = () => {
    this.setState( {
      successModal: false,
      stock: false,
      scan: true
    } );
  };

  bindInventory = ( id ) => {
    if(id !== '' && id !== undefined){
      id = id.toLowerCase();
    }else{
      return null
    }
    this.setState({stockId:this.state.stockId.toLowerCase(), inventoryId:id, inProgress: true,}) ;
    const type = 'identify_id';
    this.props.searchAction( this.state.inventoryId, type, this.props.navigation)
      .then( () => {
        this.props.checkIfMount(this.state.inventoryId)
          .then(()=>{
            if ( this.props.inventory && this.props.inventory.is_ban !== false && this.state.ifMount.data[0].is_mounted === false ) {
              this.setState( { confirmStockSharpening: true, inProgress: false, } );
            }else if(this.state.ifMount.data[0].is_mounted){
              this.setState({isMountedRestrictionWindow:true, inProgress: false, })
            }else{
              this.setState( { inProgress: true, } );
              this.bindInventoryStockSharpening()
            }
          })
      } );
  };

  confirmedBindInventory = () => {
      this.setState({ scan: true } );
      this.bindInventoryStockSharpening();
  };

  bindInventoryStockSharpening = () =>{
    if ( this.state.page && this.state.page === 'Sharpening' ) {
      this.props.bindInventoryToSharpening( {
        inventory_id: this.state.inventoryId,
        warehouse_id: this.state.stockId,
        username: this.state.mac
      } )
        .then( () => {
          this.setState( { inProgress: false, } );
            this.openNextWindow();
        } )
        .catch( err => {
          console.log( err )
        } )
    } else if ( this.state.page && this.state.page === 'Stock' ) {
      this.props.bindInventoryToWarehouse( {
        inventory_id: this.state.inventoryId,
        warehouse_id: this.state.stockId,
        username: this.state.mac
      } )
        .then( () => {
          this.setState( { inProgress: false, } );
            this.openNextWindow();
        } )
        .catch( err => {
          console.log( err )
        } )
    }
  };

  openNextWindow = () => {
    this.setState( {
      nextWindow: true,
      scan: false
    } );
  };

  resolveNextWindow = () => {
    this.setState( {
      nextWindow: false,
      scan: true
    } );
  };

  resolveconfirmStockSharpening = () => {
    this.setState( {
      confirmStockSharpening: false,
      confirmResolveWindow: true,
      scan: true
    } );
    this.bindInventoryStockSharpening()
  };

  rejectNextWindow = () => {
    this.props.navigation.navigate( 'Identification' );
    this.setState( {
      confirmStockSharpening:false,
      nextWindow: false,
      scan: true,
      stock: true,
      inProgress: false,
    } );
  };

  closeModal = () => {
    this.props.closeError();
    this.setState( {
      scan: true,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
      inProgress: false,
    } );
  };

  closeWrongWarehouseModal = ()=> {
    this.props.closeError();
    this.setState( {
      scan: true,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
      inProgress: false,
    } );
    if(this.state.page === 'Stock'){
      this.props.navigation.navigate('CommonPageWarehouse');
    }else{
      this.props.navigation.navigate('CommonPageSharpening');
    }

  };

  bindErrorNotFoundCloseModal = () => {
    this.props.closeError();
    this.setState( {
      scan: false,          //true
      checkWarehouseError: false,
      confirmStockSharpening: false,
      bindError: false,
      modal: false,
      bindErrorNotFound: false,
    } );
    if(this.state.page !== 'CommonPageWarehouse'){
      this.props.navigation.navigate('CommonPageWarehouse');
    }else{
      this.props.navigation.navigate('CommonPageSharpening');
    }
  };

  turnFlash = () => {
    if ( this.state.torch === Camera.Constants.FlashMode.torch ) {
      this.setState( { torch: Camera.Constants.FlashMode.off, flash: Camera.Constants.FlashMode.off } )
    } else {
      this.setState( { torch: Camera.Constants.FlashMode.torch, flash: Camera.Constants.FlashMode.torch } )
    }
  };

  refusedIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
    } );
  }

  confirmIsMountedRestrictionWindow() {
    this.setState( {
      isMountedRestrictionWindow: false,
      isMount: false,
    },this.confirmedBindInventory() );
  }

  showManualEnterWindow(){
    this.setState({ manualEnterWindow:true});
  }

  onChangeInventory(e){
    this.setState({inventoryId: e.trim()});
  }

  bindWarehouseManually = () => {
    this.bindInventory(this.state.inventoryId);
    this.setState({manualEnterWindow: false})
  };

  render() {
    let page;
    if ( this.props.navigation.state ) {
      page = this.props.navigation.state.routeName;
    }

    return (
      <View
        style={ { flex: 1 } }
      >
        <_Modal
          placeholder={ 'Введите Идентификатор' }
          onChange={ this.onChangeInventory.bind(this) }
          visible={this.state.manualEnterWindow}
          type="custom"
          closeModal={this.bindWarehouseManually}
        >
          Введите идентификатор ТМЦ Вручную
        </_Modal>
        <_Modal
          closeModal={this.closeModal}
          visible={this.state.modal}
          type="alert"
        >
          {`Идентификатор ${this.state.inventoryId} не найден в базе!`}
        </_Modal>

        <_Modal
          closeModal={this.bindErrorNotFoundCloseModal}
          visible={this.state.bindErrorNotFound}
          type="alert"
        >
          {`Не найден участок заточки`}
        </_Modal>

        <_Modal
          closeModal={ this.refusedIsMountedRestrictionWindow.bind(this) }
          confirmModal={ this.confirmIsMountedRestrictionWindow.bind(this) }
          visible={ this.state.isMountedRestrictionWindow }
          type="confirm"
        >
          {
            `Внимание, данное ТМЦ установлено на оборудование. В случае осуществления вами операции, оно будет демонтировано автоматически. Продолжить?`
          }
        </_Modal>
        <_Modal
          closeModal={ this.closeModal }
          visible={ this.state.bindError }
          type={ 'alert' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Деталь уже находится на данном складе' : ' Деталь уже находится на участке заточки' }
        </_Modal>
        <_Modal
          closeModal={ this.rejectNextWindow }
          confirmModal={ this.resolveconfirmStockSharpening }
          visible={ this.state.confirmStockSharpening }
          type={ 'confirm' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Деталь уже списана с производства. Хотите продолжить' : 'Деталь уже списана с производства. Хотите продолжить' }
        </_Modal>
        <_Modal
          closeModal={ this.rejectNextWindow }
          confirmModal={ this.resolveNextWindow }
          visible={ this.state.nextWindow }
          type={ 'successConfirm' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Операция Успешна!\r\nХотите ли принять другую деталь на склад?' :
            'Операция Успешна!\r\nХотите ли принять другую деталь на участок заточки?'
          }
        </_Modal>
        <_Modal
          closeModal={ this.resolveSuccessModal }
          visible={ this.state.successModal }
          type={ 'success' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Склад определен' :
            'Участок заточки определен'
          }
        </_Modal>
        <_Modal
          closeModal={ this.closeWrongWarehouseModal }
          visible={ this.state.checkWarehouseError }
          type={ 'alert' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Такого склада не существует' :
            'Такого участка заточки не существует'
          }
        </_Modal>
        { this.state.inProgress ? <Spinner/> :

          <Camera
            flashMode={ this.state.torch }
            style={ { flex: 1 } }
            onBarCodeRead={ ( { data } ) => {
              if ( this.state.scan ) {
                this.setState( { scan: false } );
                if ( this.state.stock ) {
                  this.getStock( data );
                } else {
                  this.bindInventory( data );
                }
              }
            } }
          >
            { Platform.OS === 'ios' && <ViewFinder/> }

            <React.Fragment>
              <View style={ styles.buttonsContainer }>
                <View style={ styles.buttonContainer }>
                  { this.props.navigation.state.params.warehouse_id && <Button
                    onClick={ this.showManualEnterWindow.bind( this ) }> Ввести код вручную
                  </Button> }
                </View>

                <View style={ styles.buttonContainer }>
                  <Button onClick={ this.turnFlash }> освещение </Button>
                </View>

              </View>

              { Platform.OS === 'android' && <ViewFinder/> }

              <View style={ styles.header }>
                <Text style={ styles.headerText }>
                  {
                    this.state.stock ?
                      (this.state.page && this.state.page !== 'Sharpening' ? `Отсканируйте QR-код склада` : `Отсканируйте QR-код участка заточкии`)
                      : `Отсканируйте QR-код ТМЦ`
                  }
                </Text>
              </View>
            </React.Fragment>
          </Camera>
        }
      </View>
    )
  }
}

const mapStateToProps = ( state ) => ({
  bindError: state.error.errors.bindError,
  checkWarehouseError: state.error.errors.checkWarehouseError,
  inventory: state.inventory.inventory,
  ifMount: state.inventory.ifMount,
  error: state.error.errors.identifyIdError,
  bindErrorNotFound: state.error.errors.bindErrorNotFound,
});

export default connect( mapStateToProps, {
  checkWarehouse, bindInventoryToSharpening, bindInventoryToWarehouse, checkIfMount,
  closeError, searchAction
} )( Stock )
