import React, { Component } from 'react';
import { SafeAreaView, View, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { equipmentAction } from '../../../actions/equipmentAction';
import { getInventoryFromWarehouse } from '../../../actions/searchAction';
import { closeError } from '../../../actions/errorAction';
import Header from '../../Components/Header';
import styles from './styles';

class InventoriesFromWarehouse extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      limit: 5,
      offset: 0
    };
  }

  async componentWillMount() {
    await this.setState( {
      id: this.props.navigation.getParam( 'id' ),
      title: this.props.navigation.getParam( 'itemTitle' )
    } );
    this.props.getInventoryFromWarehouse( this.state.id, 'inventory_id_from_warehouse', this.state.title.toString(),
      this.state.limit, this.state.offset )
      .then(()=>{
      });
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      inventory_id: nextProps.navigation.state.params.id,
      inProgress: false
    } );
    this.setState( {
      InventoryFromWarehouse: nextProps.InventoryFromWarehouse,
      CountInventoryFromWarehouse: nextProps.CountInventoryFromWarehouse,
    } );
  };

  async onEndFlatListReachedInventoryFromWarehouse() {
    if ( this.state.CountInventoryFromWarehouse > this.state.InventoryFromWarehouse.length ) {
      this.props.getInventoryFromWarehouse( this.state.id, 'inventory_id_from_warehouse', this.state.title.toString(),
        this.state.limit += 4, this.state.offset=0 )
        .then(()=>{
          this.setState({InventoryFromWarehouse})
        });
      return this.state.limit;
    }
  }

  getTime( date ) {
    let hours = new Date( date ).getHours().toString();
    let minutes = new Date( date ).getMinutes().toString();
    let seconds = new Date( date ).getSeconds().toString();
    if ( hours.length === 1 ) {
      hours = `0${ hours }`
    }
    if ( minutes.length === 1 ) {
      minutes = `0${ minutes }`
    }
    if ( seconds.length === 1 ) {
      seconds = `0${ seconds }`
    }
    return `${ hours }:${ minutes }:${ seconds }`
  }

  getDate( date ) {
    let year = new Date( date ).getFullYear().toString();
    let month = (new Date( date ).getMonth() + 1).toString();
    let day = new Date( date ).getDate().toString();

    if ( year.length === 1 ) {
      year = `0${ year }`
    }
    if ( month.length === 1 ) {
      month = `0${ month }`
    }
    if ( day.length === 1 ) {
      day = `0${ day }`
    }
    return `${ year }-${ month }-${ day }`

  }

  render() {

    return (
      <SafeAreaView
        style={ styles.container }
      >
        { this.state.id[0] === 'w'&& <Header page={ 'InventoriesFromWarehouse' }>  ТМЦ со склада </Header> }
        { this.state.id[0] === 'r'&& <Header page={ 'InventoriesFromWarehouse' }>  ТМЦ с участка заточки </Header> }

        { this.state.InventoryFromWarehouse && this.state.InventoryFromWarehouse !== [] &&
        <View>
          <FlatList
            data={ this.state.InventoryFromWarehouse }
            onEndReached={ this.onEndFlatListReachedInventoryFromWarehouse.bind( this ) }
            onEndThreshold={0.9}
            keyExtractor={ ( item, index ) => index.toString() }
            contentContainerStyle={ { paddingBottom: 50 } }
            renderItem={ ( { item } ) =>
              <View style={ styles.inventoryItem }>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'ID ТМЦ: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>
                    { item._id }
                  </Text>
                </View>
                <View style={ styles.serveDateContainer }>

                  <Text style={ { width: '45%' } }>{ 'Срок без движения: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>
                    { `${ item.time_transaction.days } дней ${ item.time_transaction.hours } часов ${ item.time_transaction.minutes } минут` }
                  </Text>
                </View>
                <View style={ styles.serveDateContainer }>

                  <Text style={ { width: '45%' } }>{ 'Ответственный: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>
                    {item.user.firstName !== null && item.user.lastName !== null ?
                    `${ item.user.firstName} ${item.user.lastName }` : 'Отсутсвует' }
                  </Text>
                </View>
              </View>
            }
          />
        </View>
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  InventoryFromWarehouse: state.inventory.InventoryFromWarehouse.inventories,
  CountInventoryFromWarehouse: state.inventory.InventoryFromWarehouse.count,
});

export default connect( mapStateToProps, { equipmentAction, closeError, getInventoryFromWarehouse } )(
  InventoriesFromWarehouse );
