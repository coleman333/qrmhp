import React, { Component } from 'react';
import {
  SafeAreaView, View, TextInput, TouchableOpacity, Keyboard, AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { Button } from '../../Components/Button';
import Header from '../../Components/Header';
import Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import { checkWarehouse, getTime, writeOff } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import { searchAction } from '../../../actions/searchAction';

import styles from './styles';

class WritingOffPage extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      modal: false,
      serverError: false,
      id: '',
      connection: true,
      inProgress: false,
      connectionModal: false,
      page: '',
      nameModal: false,

    };
    this.openCamera = this.openCamera.bind( this );
    this.getInfo = this.getInfo.bind( this );
    this.closeModal = this.closeModal.bind( this );
    this.getValue = this.getValue.bind( this );
    this.confirmWritingOffModal = this.confirmWritingOffModal.bind( this );
    this.chooseBreakdown = this.chooseBreakdown.bind( this );
  }

  componentDidMount() {
    const pickerArray = ['Поломка 1', 'Поломка 2', 'Поломка 3'];
    this.setState({pickerArray},()=>{console.log(pickerArray)});
    if ( this.props.navigation.state.routeName === 'CommonPageWarehouse' ) {
      this.setState( { page: 'Stock' } )
      this.setState( { test: 'test' } )
    } else {
      this.setState( { page: 'Sharpening' } )
    }
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      serverError: nextProps.serverError,
      connection: nextProps.connection,
      inProgress: false,
      checkWarehouseError: nextProps.checkWarehouseError,
      checkWarehouseResult: nextProps.checkWarehouseResult, // if warehouse exist
    } );
    if(nextProps.writeOffAlreadyMountedError){
      this.setState({ writeOffAlreadyMountedError: true})
    }
  }

  //  open camera to scan QR-code
  openCamera() {
    this.props.navigation.navigate('WriteOffCamera');
  }

  //  get info about material or equipment according to id
  getInfo() {
    let id = this.state.id;
    id = id.toLowerCase();
    this.restrictDoubleWritingOff.bind(this);
    this.setState( { confirmDeletingModal: true, id: id } );
  }

  async restrictDoubleWritingOff(){
    await this.props.searchAction( this.state.id, this.props.navigation );
  }

  confirmWritingOffModal() {
    this.setState( { confirmDeletingModal: false, nameModal: true } );
  }

  chooseBreakdown( breakdown ) {
    console.log('this is from choose Breakdown', breakdown);
    const metadata = { description: breakdown };
    this.setState( { metadata } );
  }

  sendWritingOffRequest(){
    this.setState( { nameModal: false});
    if(this.state.metadata === undefined){
      const metadata = { description: this.state.pickerArray[0] };
      this.props.writeOff( this.state.id, { metadata: metadata} )
        .then( () => {
          this.setState( {
            ...this.state,
            inProgress: false,
            writeOffConfirmationWindow: true
          } );
        } );
    }else{
      this.props.writeOff( this.state.id, { metadata: this.state.metadata} )
        .then( () => {
          this.setState( {
            ...this.state,
            inProgress: false,
            writeOffConfirmationWindow: true
          } );
        } );
    }
  }

  //  close modal window
  closeModal() {
    this.setState( {
      id: '',
      connectionModal: false,
      confirmDeletingModal: false,
    } );
    this.props.closeError();
  }

  //  get value from input
  getValue( e ) {
    this.setState( {
      id: e
    } );
  }

  render() {
    let { confirmDeletingModal, nameModal, checkWarehouseError, connectionModal, inProgress, pickerArray } = this.state;
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <TouchableOpacity
          activeOpacity={ 1.0 }
          onPress={ Keyboard.dismiss }
          style={ { flex: 1 } }
        >
          <React.Fragment>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.props.writeOffAlreadyWrittenOffError }
              type={ 'alert' }
            >
              Данное ТМЦ уже ранее было списано с производства
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              confirmModal={ this.confirmWritingOffModal }
              visible={ confirmDeletingModal }
              type="confirm"
            >
              { `Вы действительно хотите списать с производства ${ this.state.id }` }
            </Modal>
            <Modal
              pickerArray = { pickerArray }
              onChange={ breakdown => this.chooseBreakdown( breakdown ) }
              closeModal={ () => this.sendWritingOffRequest( 'breakdown' ) }
              visible={ nameModal }
              type="picker"
            />
            <Modal
              closeModal={ ()=>{this.setState({writeOffConfirmationWindow: false} ) } }
              visible={ this.state.writeOffConfirmationWindow }
              type={ 'success' }
            >
              Списание ТМЦ прошло успешно
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ checkWarehouseError }
              type={ 'alert' }
            >
              Такого склада не существует
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.props.writeOffAlreadyMountedError }
              type={ 'alert' }
            >
              ТМЦ уже монтированно
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ this.props.writeOffInventoryNotExists }
              type="alert"
            >
              { `Идентификатор ${ this.state.id } не найден в базе!` }
            </Modal>
            <Modal
              closeModal={ this.closeModal }
              visible={ connectionModal }
              type="alert"
            >
              Отсутствует интернет соединение
            </Modal>
            <Header>
              Списание ТМЦ
            </Header>
            {
              inProgress
                ? <Spinner/>
                : (
                  <React.Fragment>
                    <View
                      style={ styles.item }
                    >
                      <View
                        style={ styles.inputWithButton }
                      >
                        <TextInput
                          value={ this.state.id }
                          onChangeText={ this.getValue }
                          placeholder={ 'введите идентификатор' }
                          style={ styles.input }
                        />
                        <Button onClick={ this.getInfo }> Отправить </Button>
                      </View>
                    </View>
                    <View
                      style={ styles.item }
                    >
                      <Button
                        onClick={ this.openCamera }
                      >
                        Открыть камеру
                      </Button>
                    </View>
                  </React.Fragment>
                )
            }
          </React.Fragment>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.identifyIdError,
  serverError: state.error.errors.identifyServerError,
  connection: state.connection.connection,
  checkWarehouseError: state.error.errors.checkWarehouseError,
  checkWarehouseResult: state.inventory.checkWarehouseResult,
  writeOffAlreadyMountedError: state.error.errors.writeOffAlreadyMountedError,
  inventory: state.inventory.inventory,
  writeOffAlreadyWrittenOffError: state.error.errors.writeOffAlreadyWrittenOffError,
  writeOffInventoryNotExists: state.error.errors.writeOffInventoryNotExists,

});

export default connect( mapStateToProps, { writeOff, checkWarehouse, closeError, getTime, searchAction } )( WritingOffPage );
