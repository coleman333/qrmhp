import React, { Component } from 'react';
import { SafeAreaView, View, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import _Button from '../../Components/Button';
import Spinner from '../../Components/ActivityIndicator';
import styles from './styles';
import { getAllUnmarkedInventoryAction, markingAction, markDetail, markeratorRequest } from '../../../actions/markingAction';
import { closeError } from '../../../actions/errorAction';

class StartPage extends Component {

  state = {
    modal: false,
    confirmModal: false,
    confirmMarkModal: false,
    successModal: false,
    amount: 0,
    inProgress: false,
    remarkModal: false,
    nextModal: false,
    id: '',
    item: null,
    items: [],
    port: '',
    host: ''
  };

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      items: nextProps.marking,
      allUnmarkedInventory: nextProps.allUnmarkedInventory,
      inProgress: false
    } );
  };

  async componentDidMount(){
    //get all unmarked details
    await this.props.getAllUnmarkedInventoryAction();
    await this.setState({allUnmarkedInventory: this.props.allUnmarkedInventory });
  }

  async chosenKnifeType (type){
    const knifeType = await AsyncStorage.setItem( 'knifeType',type );
   this.props.navigation.navigate('Identification');
  }

  render() {
    const { items, amount } = this.state;

    return (
      <SafeAreaView
        style={ styles.container }
      >
         <Header>
          Выберите тип ножей
        </Header>

        {
          this.state.inProgress ? <Spinner/> :

            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <View style={{width: '100%'}}>
                <_Button onClick={ this.chosenKnifeType.bind(this,'manual') } > ручные </_Button>
              </View>
              <View style={{width: '100%', marginTop: '10%'}}>
                <_Button onClick={ this.chosenKnifeType.bind(this, 'automatic') } > автоматические </_Button>
              </View>
            </View>
        }
      </SafeAreaView>
    )
  }
}

const mapStateToProps = state => ({
  error: state.error.errors.markingError,
  marking: state.marking.marking,
  allUnmarkedInventory: state.marking.allUnmarkedInventory
});

export default connect( mapStateToProps, { getAllUnmarkedInventoryAction, markingAction, closeError, markDetail, markeratorRequest } )( StartPage );
