import React, { Component } from 'react';
import {
  SafeAreaView, View, Text, FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { equipmentAction } from '../../../actions/equipmentAction';
import { getHistory } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import Header from '../../Components/Header';
import styles from './styles';

class History extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      limit: 10,
      offset: 0
    };

  }

  async componentDidMount() {
    await this.setState( { id: this.props.navigation.state.params } );
    await this.props.getHistory( this.state.id, this.state.limit, this.state.offset );
  }

  componentWillReceiveProps( nextProps ) {
    this.setState( {
      modal: nextProps.error,
      inventory_id: nextProps.navigation.state.params.id,
      inProgress: false
    } );
    if ( nextProps.history !== null ) {
      this.setState( { history: nextProps.history.data.transactions, count: nextProps.history.data.count, } )
    }
  };

  async onEndFlatListReached() {
    console.log( 'this is the end of the flat list', this.state.count[0].count );

    if ( this.state.count[0].count >= this.state.history.length ) {
      await this.props.getHistory( this.state.id, this.state.limit += 10, this.state.offset = 0 )
    }
  }

  getTime( date ) {
    let hours = new Date( date ).getHours().toString();
    let minutes = new Date( date ).getMinutes().toString();
    let seconds = new Date( date ).getSeconds().toString();
    if ( hours.length === 1 ) {
      hours = `0${ hours }`
    }
    if ( minutes.length === 1 ) {
      minutes = `0${ minutes }`
    }
    if ( seconds.length === 1 ) {
      seconds = `0${ seconds }`
    }
    return `${ hours }:${ minutes }:${ seconds }`
  }

  getDate(date){
    let year = new Date( date ).getFullYear().toString();
    let month = (new Date( date ).getMonth() + 1).toString();
    let day = new Date( date ).getDate().toString();

    if ( year.length === 1 ) {
      year = `0${ year }`
    }
    if ( month.length === 1 ) {
      month = `0${ month }`
    }
    if ( day.length === 1 ) {
      day = `0${ day }`
    }
    return `${ year }-${ month }-${ day }`

  }

  render() {
    let history = this.state.history;
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <Header page={'history'}> История транзакций </Header>

        <View>
          <FlatList
            data={ history }
            onEndReached={ this.onEndFlatListReached.bind( this ) }
            onEndThreshold={0.9}
            keyExtractor={ ( item, index ) => index.toString() }
            contentContainerStyle={ { paddingBottom: 50 } }
            renderItem={ ( { item } ) =>
              <View style={ styles.inventoryItem }>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Транзакция: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ item.transaction_name } </Text>
                </View>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Дата: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }> { this.getDate(item.date) } </Text>
                </View>
                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Время: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>{ this.getTime( item.date ) } </Text>
                </View>

                <View style={ styles.serveDateContainer }>
                  <Text style={ { width: '45%' } }>{ 'Сотрудник: ' } </Text>
                  <Text style={ { width: '55%', textAlign: 'right' } }>
                    { item.user.firstName !== null && item.user.lastName !== null ?
                      `${ item.user.firstName } ${ item.user.lastName }` : 'Отсутсвует' } </Text>
                </View>
              </View>
            }
          />
        </View>

      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  history: state.inventory.history
});

export default connect( mapStateToProps, { equipmentAction, closeError, getHistory } )(
  History );
