import { StyleSheet } from 'react-native';

import Colors from '../../Constants/Colors';
import Others from '../../Constants/Other';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginBottom: 20
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    height: 50,
    borderColor: Colors.grayBorder,
    borderRadius: Others.borderRadius,
    paddingHorizontal: 10,
    marginVertical: 5
  },

  innerViewItem:{
    flexDirection: 'row',
    borderBottomColor: Colors.grayBorder,
    borderBottomWidth: 1,
    height: 40,
    alignItems: 'center'
  },
  innerTextItem:{
    width: '55%', textAlign: 'right'
  },
  button: {
    borderWidth: 1
  },
  buttonText: {
    textAlign: 'center'
  }
});
