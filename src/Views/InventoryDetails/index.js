import React, { Component } from 'react';
import { SafeAreaView, View, Text, ScrollView, Linking } from 'react-native';
import { connect } from 'react-redux';
import { equipmentAction } from '../../../actions/equipmentAction';
import { getManualUrl } from '../../../actions/inventoryAction';
import { closeError } from '../../../actions/errorAction';
import Header from '../../Components/Header';
import { Button } from '../../Components/Button';
import Modal from '../../Components/Modal';
import styles from './styles';

class InventoryDetails extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      modal: false,
      mount: false,
      unmount: false,
      modalText: ['', '']
    };
    this.mount = this.mount.bind( this );
    this.unmount = this.unmount.bind( this );
    this.refusedModal = this.refusedModal.bind( this );
    this.confirmModal = this.confirmModal.bind( this );
    this.openCamera = this.openCamera.bind( this );
  }

  //  mount detail to equipment
  mount() {
    this.setState( {
      modal: true,
      mount: true,
      modalText: ['монтировать', 'на']
    } );
  }

  //  unmount detail from equipment
  unmount() {
    this.setState( {
      modal: true,
      unmount: true,
      modalText: ['демонтировать', 'из']
    } );
  }

  //  event click button 'CANCEL' on confirm modal mount or unmount detail from equipment
  refusedModal() {
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
  }

  //  event click button 'OK' on confirm modal mount or unmount detail from equipment
  confirmModal() {
    this.setState( {
      modal: false,
      mount: false,
      unmount: false
    } );
    this.state.mount ? this.openCamera( 'mount', this.props.inventory.id ) //   if user want mount detail
      : this.state.unmount ? this.openCamera( 'unmount', this.props.inventory.id, this.props.inventory.equipment.id ) : null; //  if use want unmount detail
  }

  //  open camera for scanning QR-code of equipment where details is installed
  openCamera( type, inventoryId, equipment_id ) {
    this.props.navigation.navigate( 'Qrscaner', {
      text: 'Отсканируйте QR-код оборудования', type, inventoryId, equipment_id
    } );
  }

  redirectToManualUrl() {
    const { manualUrl } = this.props;
    Linking.openURL( manualUrl.data )
  }

  redirectToHistory() {
    this.props.navigation.navigate( 'HistoryPage', this.props.inventory.id )
  }

  redirectToManualLinksPromptsUrlsPage(){
    this.props.navigation.navigate('LinksOnVideoPrompts', { id: this.props.inventory.id, title: this.props.inventory.title });
    this.setState({inProgress: true});
  }

  redirectToMountingPromptsPage(){
    this.props.navigation.navigate('MountingPrompts', {id: this.props.inventory.id, title: this.props.inventory.title});
  }

  render() {
    console.log();
    const { manualUrl } = this.props;
    this.buttons = this.props.navigation.getParam( 'buttons' );
    const { keysMetadata } = this.props.inventory.metadata;
    const transactions = this.props.inventory.transactions
      .sort( ( a, b ) => new Date( a.created_at ).getTime() - new Date( b.created_at ).getTime() );
    const valuesMetadata = [...this.props.inventory.metadata.valuesMetadata];
    valuesMetadata[7] = new Date( valuesMetadata[7] ).toLocaleDateString();
    const mount = (
      <View>
        {
          this.buttons
            ? (
              <View>
                <View style={ { marginTop: '1%' } }>
                  <Button
                    onClick={ this.unmount }
                  >
                    Демонтировать
                  </Button>
                </View>
              </View>
            )
            : null
        }
      </View>
    );

    const unmount = (
      <View>

        {
          this.buttons
            ? (
              <View>
                <View>
                </View>
                { this.props.inventory.is_ban === false && <View style={ { marginTop: '1%' } }>
                  <Button
                    onClick={ this.mount }
                  >
                    Монтировать
                  </Button>
                </View> }

              </View>
            )
            : null
        }
      </View>
    );

    const inventoryName = this.props.inventory.title;
    const inventory = this.props.inventory;
    let equipment = {};
    if ( this.props.inventory.equipment ) {
      equipment = this.props.inventory.equipment
    }
    const modalText = `${ this.state.modalText[1] } ${ this.props.inventory.equipment ? this.props.inventory.equipment.name : null }`;
    const is_ban = this.props.inventory.is_ban;
    let id;
    if ( this.props.inventory && this.props.inventory.status ) id = this.props.inventory.status.name;
    return (
      <SafeAreaView
        style={ styles.container }
      >
        <Header>
          {
            this.buttons ? `Монтаж/Демонтаж` : `Детальная информация о ТМЦ`
          }
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={ false }
        >
          <Modal
            closeModal={ this.refusedModal }
            confirmModal={ this.confirmModal }
            visible={ this.state.modal }
            type="confirm"
          >
            { `Вы действительно хотите ${ this.state.modalText[0] } ${ inventoryName } ${ id === 'mounting' ? modalText : '' }` }
          </Modal>
          <View>
            {
              // Fields.map( item => (
              //   <View key={ item } style={ styles.item }>
              //     <Text style={ { width: '45%' } }> { keysMetadata[item] } </Text>
              //     <Text style={ { width: '55%', textAlign: 'right' } }> { valuesMetadata[item] } </Text>
              //   </View>
              // ) )
            }
            { is_ban === false && id === 'mounting' && <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Установлено на оборудование: </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }>
                { equipment.name } </Text>
            </View> }
            { is_ban === false && id !== 'mounting' && <View style={ styles.item }>
              <Text style={ { width: '100%' } }> Не установлено на оборудование </Text>
            </View> }
            { is_ban && <View style={ styles.item }>
              <Text style={ { width: '100%', textAlign: 'center' } }> ТМЦ списано с производства </Text>
            </View> }

            <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Номенклатура </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> { inventoryName } </Text>
            </View>
            <View style={ styles.item }>
              <Text style={ { width: '45%' } }> ID ножа </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> { inventory.id } </Text>
            </View>
            { inventory.amount_time && <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Общее время работы детали </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> {
                `${ inventory.amount_time.days } дней ${ inventory.amount_time.hours } часов ${ inventory.amount_time.minutes } минут`
              } </Text>
            </View> }
            { inventory.last_transaction_time && <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Время работы детали </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> {
                `${ inventory.last_transaction_time.days } дней ${ inventory.last_transaction_time.hours } часов ${ inventory.last_transaction_time.minutes } минут`
              } </Text>
            </View> }
            <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Код ЦБ </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> { inventory.metadata.valuesMetadata[2] } </Text>
            </View>
            <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Поставщик </Text>
              <Text style={ { width: '55%', textAlign: 'right' } }> { inventory.metadata.valuesMetadata[6] } </Text>
            </View>
            <View style={ styles.item }>
              <Text style={ { width: '45%' } }> Дата Покупки </Text>
              {inventory.metadata.keysMetadata[5] === "Дата покупки (дата поставки на склад)" ?
              <Text style={ { width: '55%', textAlign: 'right' } }>
              { inventory.metadata.valuesMetadata[5] }
                </Text>
                : <Text style={ { width: '55%', textAlign: 'right' } }>
                  { inventory.metadata.valuesMetadata[10] }
                </Text>
              }
            </View>
          </View>
          {
            id === 'mounting' ? mount : unmount
          }
          <View>
            <View style={ { marginTop: '1%' } }>
              <Button onClick={ this.redirectToHistory.bind( this ) } > История </Button>
            </View>
            }
            {
              (this.props.navigation.getParam( 'type' ) === 'identify_id') &&
                  <View style={ { marginTop: '1%' } }>
                    <Button onClick={ this.redirectToManualLinksPromptsUrlsPage.bind( this ) }>Список видео</Button>
                  </View>
            }
            {
              !(this.props.navigation.getParam( 'identificationFlag' ) ) &&
              <View style={ { marginTop: '1%' } }>
                <Button onClick={ this.redirectToMountingPromptsPage.bind( this ) }>Варианты монтажа</Button>
              </View>
            }
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  inventory: state.inventory.inventory,
  equipment: state.equipment.equipment,
  time: state.inventory.time,
  manualUrl: state.inventory.manualUrl
});

export default connect( mapStateToProps, { getManualUrl, equipmentAction, closeError } )( InventoryDetails );
