import React, { Component } from 'react';
import { Keyboard, SafeAreaView, TextInput, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Header from '../../Components/Header';
import _Button from '../../Components/Button';
import _Modal from '../../Components/Modal'
import Spinner from '../../Components/ActivityIndicator';
import styles from './styles';
import { searchAction } from "../../../actions/searchAction";
import { getTime } from "../../../actions/inventoryAction";
import { closeError } from "../../../actions/errorAction";

class Transaction extends Component{

    state = {
        modal: false,
        serverError: false,
        id: '',
        connection: true,
        inProgress: false,
        connectionModal: false
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            modal: nextProps.error,
            serverError: nextProps.serverError,
            connection: nextProps.connection,
            inProgress: false
        })
    };

    //  get info and settings for equipment or details according to id
    getInfo = () => {
        if(this.state.id.length){
            if(this.state.connection) {
                this.setState({
                    inProgress: true
                });
                this.props.searchAction(this.state.id.toLowerCase(), 'transaction_id', true, this.props.navigation)
                    .then(() => {
                        this.setState({
                            inProgress: false
                        });
                    });
                this.props.getTime(this.state.id);
            } else {
                this.setState({
                    connectionModal: true
                });
            }
        }
    };

    //  get value from input
    getValue = (e) => {
        this.setState({
            id: e
        })
    };

    //  open camera for scan QR-code
    openCamera = () => {
        this.props.navigation.navigate('Qrscaner', { text: 'Отсканируйте QR-код детали', type: 'search', buttons: true });
    };

    closeModal = () => {
        this.setState({
            id: '',
            connectionModal: false
        });
        this.props.closeError();
    };

    render() {
        return(
            <SafeAreaView
                style={styles.container}
            >
                <TouchableOpacity
                    activeOpacity={1.0}
                    onPress={Keyboard.dismiss}
                    style={{flex: 1}}
                >
                    <React.Fragment>
                        <_Modal
                            closeModal={this.closeModal}
                            visible={this.state.modal}
                            type={'alert'}
                        >
                            {`Идентификатор ${this.state.id} не найден в базе!`}
                        </_Modal>
                        <_Modal
                            closeModal={this.closeModal}
                            visible={this.state.connectionModal}
                            type={'alert'}
                        >
                            Отсутствует интернет соединение
                        </_Modal>

                        <Header>
                            Монтаж/Демонтаж
                        </Header>
                        {
                            this.state.inProgress ?
                                <Spinner/> :
                                <React.Fragment>
                                    <View
                                        style={styles.item}
                                    >
                                        <View
                                            style={styles.inputWithButton}
                                        >
                                            <TextInput
                                                value={this.state.id}
                                                onChangeText={this.getValue}
                                                placeholder={'введите идентификатор'}
                                                style={styles.input}
                                            />
                                            <_Button
                                                onClick={this.getInfo}
                                            >
                                                Отправить
                                            </_Button>
                                        </View>
                                    </View>
                                    <View
                                        style={styles.item}
                                    >
                                        <_Button
                                            onClick={this.openCamera}
                                        >
                                            Открыть камеру
                                        </_Button>
                                    </View>
                                </React.Fragment>
                        }
                    </React.Fragment>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => ({
    error: state.error.errors.transactionIdError,
    serverError: state.error.errors.transactionServerError,
    connection: state.connection.connection
});

export default connect(mapStateToProps, { searchAction, closeError, getTime })(Transaction);
