import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { Calendar } from 'react-native-calendars';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getReport } from '../../../actions/reportAction';
import Colors from '../../Constants/Colors';
import { Button } from '../../Components/Button';
import Modal from '../../Components/Modal';
import styles from '../Identification/styles';
import Header from '../../Components/Header';

const oneDay = 24 * 60 * 60 * 1000;
const style = {
  customStyles: {
    container: {
      backgroundColor: Colors.mhp_blue
    },
    text: {
      color: Colors.gray,
      fontWeight: 'bold'
    }
  }
};
export class Report extends Component {
  constructor (props) {
    super(props);
    this.state = {
      marked: null,
      startDate: null,
      finishDate: null,
      alertModal: false,
      successModal: false,
      serverModal: false
    };
    this.dayHandler = this.dayHandler.bind(this);
    this.getReport = this.getReport.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  getReport () {
    const { startDate, finishDate } = this.state;
    const { getReport: GetReport } = this.props;
    const diff = new Date(finishDate).getTime() - new Date(startDate).getTime();
    let data;
    if (!startDate && !finishDate) {
      this.setState({
        alertModal: true
      });
      return;
    }
    if (!finishDate) {
      data = {
        date_start: startDate,
        date_end: startDate,
        offset: 0,
        limit: 10
      };
    } else {
      data = {
        date_start: diff >= 0 ? startDate : finishDate,
        date_end: diff >= 0 ? finishDate : startDate,
        offset: 0,
        limit: 10
      };
    }
    GetReport(data)
      .then(() => {
        this.setState({
          successModal: true
        });
      })
      .catch(() => {
        this.setState({
          serverModal: true
        });
      });
  }

  static getDateString (timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    let dateString = `${year}-`;
    if (month < 10) {
      dateString += `0${month}-`;
    } else {
      dateString += `${month}-`;
    }
    if (day < 10) {
      dateString += `0${day}`;
    } else {
      dateString += day;
    }
    return dateString;
  }

  closeModal () {
    this.setState({
      alertModal: false,
      successModal: false,
      serverModal: false,
      marked: null,
      startDate: null,
      finishDate: null
    });
  }

  dayHandler (day) {
    const { startDate, finishDate } = this.state;
    if ((!startDate && !finishDate) || (startDate && finishDate)) {
      this.setState({
        startDate: new Date(day.dateString),
        finishDate: null,
        marked: {
          [day.dateString]: style
        }
      });
    } else {
      const finishTimestamp = new Date(day.dateString).getTime();
      let startTimestamp = new Date(startDate).getTime();
      let diff = Math.abs(finishTimestamp - startTimestamp);
      const markedObject = {};
      markedObject[day.dateString] = style;
      while (diff > 0) {
        markedObject[Report.getDateString(startTimestamp)] = style;
        diff = Math.abs(finishTimestamp - startTimestamp);
        if (finishTimestamp > startTimestamp) {
          startTimestamp += oneDay;
        } else {
          startTimestamp -= oneDay;
        }
      }
      this.setState({
        finishDate: new Date(day.dateString),
        marked: markedObject
      });
    }
  }

  render () {
    const {
      marked, alertModal, successModal, serverModal
    } = this.state;
    return (
      <SafeAreaView
        style={styles.container}
      >
        <Header>
          Отправка отчета
        </Header>
        <Modal
          closeModal={this.closeModal}
          visible={alertModal}
          type="alert"
        >
          {
            `Выберите дату для составления отчета`
          }
        </Modal>
        <Modal
          closeModal={this.closeModal}
          visible={successModal}
          type="success"
        >
          {
            `Отчет отправлен успешно`
          }
        </Modal>
        <Modal
          closeModal={this.closeModal}
          visible={serverModal}
          type="alert"
        >
          {
            `Не удалось отправить отчет`
          }
        </Modal>
        <Calendar
          markedDates={marked}
          markingType="custom"
          onDayPress={this.dayHandler}
          onDayLongPress={this.dayHandler}
          maxDate={new Date()}
          theme={{
            todayTextColor: Colors.mhp_blue,
            arrowColor: Colors.mhp_blue
          }}
        />
        <Button
          onClick={this.getReport}
        >
          Отправить отчет
        </Button>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  report: state.report
});

Report.defaultProps = {
  getReport () {

  }
};

Report.propTypes = {
  getReport: PropTypes.func
};

export default connect(mapStateToProps, { getReport })(Report);
